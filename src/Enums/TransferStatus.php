<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SUCCESS()
 */
final class TransferStatus extends Enum
{
    public const SUCCESS = 'success';
    public const FAILED = 'failed';
    public const ERROR = 'error';
}
