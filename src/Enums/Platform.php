<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static KUNA()
 * @method static static BINANCE()
 * @method static static WHITEBIT()
 * @method static static NONE()
 */
final class Platform extends Enum
{
    public const KUNA = 'kuna';
    public const BINANCE = 'binance';
    public const WHITEBIT = 'whitebit';
    public const NONE = '---';
}
