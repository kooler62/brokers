<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ACTIVATED()
 */
final class CodeStatus extends Enum
{
    public const ACTIVATED = 'activated';
    public const FAILED = 'failed';
    public const UNCONFIRMED = 'unconfirmed';
    public const CANCELED = 'canceled';
    public const ERROR = 'error';
    public const NOT_FOUND = 'not_found';
}
