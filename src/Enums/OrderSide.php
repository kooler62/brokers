<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static BUY()
 * @method static static SELL()
 * @method static static EMPTY()
 */
final class OrderSide extends Enum
{
    public const BUY = 'buy';
    public const SELL = 'sell';
    public const EMPTY = '-';
}
