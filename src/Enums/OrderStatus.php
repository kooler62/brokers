<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ACTIVE()
 * @method static static CANCELED()
 * @method static static PARTIAL()
 * @method static static FILLED()
 * @method static static EXPIRED()
 * @method static static REJECTED()
 * @method static static CLOSE_AWAITING()
 * @method static static DETAILS_AWAITING()
 * @method static static EMPTY_DETAILS()
 */
final class OrderStatus extends Enum
{
    public const ACTIVE = 'active';
    public const CANCELED = 'canceled';
    public const PARTIAL = 'partial';
    public const FILLED = 'filled';

    public const EXPIRED = 'expired';
    public const REJECTED = 'rejected';

    public const CLOSE_AWAITING = 'close_awaiting';
    public const DETAILS_AWAITING = 'details_awaiting';
    public const EMPTY_DETAILS = 'empty_details';
}
