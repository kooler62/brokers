<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MAIN()
 * @method static static SPOT()
 * @method static static MARGIN_CROSS()
 * @method static static MARGIN_ISOLATED()
 * @method static static STAKING()
 * @method static static FUTURE()
 */
final class MarketType extends Enum
{
    public const MAIN = 'main';
    public const SPOT = 'spot';
    public const MARGIN_CROSS = 'margin_cross';
    public const MARGIN_ISOLATED = 'margin_isolated';
    public const STAKING = 'staking';
    public const FUTURE = 'future';
}
