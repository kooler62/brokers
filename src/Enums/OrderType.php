<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static UNKNOWN()
 * @method static static LIMIT()
 * @method static static MARKET()
 *
 * @method static static STOP_LIMIT()
 * @method static static STOP_MARKET()
 */
final class OrderType extends Enum
{
    public const UNKNOWN = 'unknown';
    public const LIMIT = 'limit';
    public const MARKET = 'market';

    public const STOP_LIMIT = 'stop_limit';
    public const STOP_MARKET = 'stop_market';

    //whitebit
    public const STOCK_MARKET = 'stock market';
    public const MARGIN_MARKET = 'margin market';
    public const MARGIN_LIMIT = 'margin limit';
}
