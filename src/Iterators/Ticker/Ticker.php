<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Ticker;

class Ticker
{
    private string $market;
    private float $bidPrice;
    private float $askPrice;
    private float $highPrice;
    private float $lowPrice;
    private float $lastPrice;
    private ?float $bidVolume;
    private ?float $askVolume;
    private ?float $priceChangePercent;
    private ?float $priceChange;

    public function __construct(array $data)
    {
        $this->market = $data['market'];
        $this->bidPrice = (float)$data['bidPrice'];
        $this->askPrice = (float)$data['askPrice'];
        $this->bidVolume = isset($data['bidVolume']) ? (float)$data['bidVolume'] : null;
        $this->askVolume = isset($data['askVolume']) ? (float)$data['askVolume'] : null;
        $this->highPrice = (float)$data['highPrice'];
        $this->lowPrice = (float)$data['lowPrice'];
        $this->lastPrice = (float)$data['lastPrice'];
        $this->priceChange = isset($data['priceChange']) ? (float)$data['priceChange'] : null;
        $this->priceChangePercent = isset($data['priceChangePercent']) ? (float)$data['priceChangePercent'] : null;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function getBidPrice(): float
    {
        return $this->bidPrice;
    }

    public function getAskPrice(): float
    {
        return $this->askPrice;
    }

    public function getHighPrice(): float
    {
        return $this->highPrice;
    }

    public function getLowPrice(): float
    {
        return $this->lowPrice;
    }

    public function getLastPrice(): float
    {
        return $this->lastPrice;
    }

    public function getBidVolume(): ?float
    {
        return $this->bidVolume;
    }

    public function getAskVolume(): ?float
    {
        return $this->askVolume;
    }

    public function getPriceChangePercent(): ?float
    {
        return $this->priceChangePercent;
    }

    public function getPriceChange(): ?float
    {
        return $this->priceChange;
    }
}
