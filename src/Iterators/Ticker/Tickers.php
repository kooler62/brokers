<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Ticker;

class Tickers implements \IteratorAggregate, \Countable
{
    private int $count;
    /** @var Ticker[] */
    private array $tickers;

    public function __construct(int $count, array $tickers)
    {
        $this->count = count($tickers);

        /** @var Ticker $ticker */
        foreach ($tickers as $ticker) {
            $this->tickers[] = new Ticker([
                'market' => $ticker->getMarket(),
                'bidPrice' => $ticker->getBidPrice(),
                'bidVolume' => $ticker->getBidVolume(),
                'askPrice' => $ticker->getAskPrice(),
                'askVolume' => $ticker->getAskVolume(),
                'lastPrice' => $ticker->getLastPrice(),
                'highPrice' => $ticker->getHighPrice(),
                'lowPrice' => $ticker->getLowPrice(),
                'priceChange' => $ticker->getPriceChange(),
                'priceChangePercent' => $ticker->getPriceChangePercent(),
            ]);
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Ticker[]
     */
    public function getTickers(): array
    {
        return $this->tickers;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->tickers);
    }

    public function count(): int
    {
        return count($this->tickers);
    }
}
