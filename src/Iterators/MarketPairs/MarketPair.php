<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\MarketPairs;

class MarketPair
{
    private string $symbol;
    private ?string $baseCurrency;
    private ?string $quoteCurrency;


    public function __construct(array $data)
    {
        $this->symbol = $data['symbol'];
        $this->baseCurrency = $data['baseCurrency'] ?? null;
        $this->quoteCurrency = $data['quoteCurrency'] ?? null;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function getBaseCurrency(): ?string
    {
        return $this->baseCurrency;
    }

    public function getQuoteCurrency(): ?string
    {
        return $this->quoteCurrency;
    }
}
