<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\MarketPairs;

use ArrayIterator;

class MarketPairs implements \IteratorAggregate, \Countable
{
    private int $count;
    /** @var MarketPair[] */
    private array $marketPairs;

    public function __construct(array $marketPairs)
    {
        $this->count = count($marketPairs);

        foreach ($marketPairs as $pair) {
            $this->marketPairs[] = new MarketPair((array)$pair);
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return MarketPair[]
     */
    public function getMarketPairs(): array
    {
        return $this->marketPairs;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->marketPairs);
    }

    public function count(): int
    {
        return count($this->marketPairs);
    }
}
