<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\OrderBook;

use ArrayIterator;

class OrderBookItems implements \IteratorAggregate, \Countable
{
    private int $count;
    /** @var OrderBookItem[] */
    private array $orderBookItems;
    //private float $totalVolume = 0.0;
    //private float $totalFunds = 0.0;
    //private float $avgPrice = 0.0;

    public function __construct(array $orderBookItems)
    {
        $this->count = count($orderBookItems);

        /** @var OrderBookItem $orderBookItem */
        foreach ($orderBookItems as $key => $orderBookItem) {
            $volume = $orderBookItem->getVolume();
            $price = $orderBookItem->getPrice();

            $funds = $volume * $price;
            //$this->totalVolume += $volume;
            //$this->totalFunds += $funds;
            $this->orderBookItems[] = $orderBookItem;
            //if ($key === $this->count - 1) {
            //    $this->avgPrice = $this->totalFunds / $this->totalVolume;
            //}
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return OrderBookItem[]
     */
    public function getOrderBookItems(): array
    {
        return $this->orderBookItems;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->orderBookItems);
    }

    public function count(): int
    {
        return count($this->orderBookItems);
    }
}
