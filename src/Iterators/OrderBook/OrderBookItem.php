<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\OrderBook;

class OrderBookItem
{
    public function __construct(private readonly float $price, private readonly float $volume)
    {
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }
}
