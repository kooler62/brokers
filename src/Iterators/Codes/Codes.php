<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Codes;

use ArrayIterator;
use IteratorAggregate;

class Codes implements IteratorAggregate
{
    private array $codes = [];

    public function __construct(array $codes)
    {
        foreach ($codes as $code) {
            $this->codes[] = $code;
        }
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->codes);
    }
}
