<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Codes;

class Code
{
    private string|int|null $id;
    private string $code;
    private string $currency;
    private float $amount;
    private string $status;
    private ?string $recipient;
    private ?string $publicComment;
    private ?string $privateComment;
    private string|int|null $codeCreatedAt;

    public function __construct(array $data)
    {
        $this->id = $data['id'] ?? null;
        $this->code = $data['code'];
        $this->currency = $data['currency'];
        $this->amount = (float)$data['amount'];
        $this->status = $data['status'];
        $this->recipient = $data['recipient'] ?? null;
        $this->publicComment = $data['public_comment'] ?? null;
        $this->privateComment = $data['private_comment'] ?? null;
        //  "sn" => "Ge3B5XcczooL"
        //  "non_refundable_before" => null
        //  "redeemed_at" => null
        $this->codeCreatedAt = $data['code_created_at'] ?? null;
    }

    public function getId(): int|string|null
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function getPublicComment(): ?string
    {
        return $this->publicComment;
    }

    public function getPrivateComment(): ?string
    {
        return $this->privateComment;
    }

    public function getCodeCreatedDate(): string|int|null
    {
        return $this->codeCreatedAt;
    }
}
