<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators;

class AccountInfo
{
    private float $makerFee;
    private float $takerFee;

    public function __construct(array $data)
    {
        $this->makerFee = (float)$data['maker_fee'];
        $this->takerFee = (float)$data['taker_fee'];
    }

    public function getMakerFee(): float
    {
        return $this->makerFee;
    }

    public function getTakerFee(): float
    {
        return $this->takerFee;
    }
}
