<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Orders;

class Orders implements \IteratorAggregate, \Countable
{
    private int $count;
    /** @var Order[] */
    private array $orders;

    public function __construct(int $count, array $orders)
    {
        $this->count = count($orders);

        foreach ($orders as $order) {
            $this->orders[] = new Order($order);
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Order[]
     */
    public function getOrders(): array
    {
        return $this->orders;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->orders);
    }

    public function count(): int
    {
        return count($this->orders);
    }
}
