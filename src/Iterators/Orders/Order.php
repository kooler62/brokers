<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Orders;

use Kooler62\Brokers\Enums\OrderSide;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;

class Order
{
    private string $orderId;
    private ?string $customOrderId;
    private float $price;
    private float $volume;
    private ?float $executedVolume;
    private string $market;
    private OrderSide $side;
    private OrderType $type;
    private OrderStatus $status;

    public function __construct(array $data)
    {
        $this->orderId = $data['order_id'];
        $this->customOrderId = $data['custom_order_id'] ?? null;
        $this->market = $data['market'];

        $this->side = OrderSide::fromValue($data['side']);
        $this->type = OrderType::fromValue($data['type']);
        $this->status = OrderStatus::fromValue($data['status']);

        $this->price = (float)$data['price'];
        $this->volume = (float)$data['volume'];
        $this->executedVolume = (float)$data['executed_volume'] ?? null;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getCustomOrderId(): ?string
    {
        return $this->customOrderId;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function getExecutedVolume(): ?float
    {
        return $this->executedVolume;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function getSide(): OrderSide
    {
        return $this->side;
    }

    public function getType(): OrderType
    {
        return $this->type;
    }

    public function getStatus(): OrderStatus
    {
        return $this->status;
    }
}
