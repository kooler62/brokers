<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\MarginCross;

use ArrayIterator;
use IteratorAggregate;

class CrossBalances implements IteratorAggregate
{
    /** @var CrossBalance[] */
    private array $balances = [];

    public function __construct(CrossBalance ...$balances)
    {
        $this->balances = $balances;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->balances);
    }
}
