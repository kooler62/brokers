<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\MarginCross;

class CrossBalance
{
    private string $currency;
    private float $balance;
    private float $available;
    private float $borrowed;
    private float $interest;

    public function __construct(string $currency, float $balance, float $available, float $borrowed, float $interest)
    {
        $this->currency = $currency;
        $this->balance = $balance;
        $this->available = $available;
        $this->borrowed = $borrowed;
        $this->interest = $interest;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getAvailable(): float
    {
        return $this->available;
    }

    public function getBorrowed(): float
    {
        return $this->borrowed;
    }

    public function getInterest(): float
    {
        return $this->interest;
    }
}
