<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Spot;

class SpotBalance
{
    public function __construct(
        private readonly float $balance,
        private readonly float $available,
        private readonly float $order,
        private readonly string $currency
    ) {
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getAvailable(): float
    {
        return $this->available;
    }

    public function getOrder(): float
    {
        return $this->order;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
