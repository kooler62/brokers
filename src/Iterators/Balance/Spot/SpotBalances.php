<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Spot;

use ArrayIterator;

class SpotBalances implements \IteratorAggregate, \Countable
{
    /**
     * @var SpotBalance[]
     */
    private array $spotBalances;

    /**
     * Balances constructor.
     * @param SpotBalance ...$spotBalances
     */
    public function __construct(SpotBalance ...$spotBalances)
    {
        $this->spotBalances = $spotBalances;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->spotBalances);
    }

    public function count(): int
    {
        return count($this->spotBalances);
    }
}
