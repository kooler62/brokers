<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Earn;

use ArrayIterator;

class EarnBalances implements \IteratorAggregate, \Countable
{
    /**
     * @var EarnBalance[]
     */
    private array $earnBalances;

    /**
     * Balances constructor.
     * @param EarnBalance ...$earnBalances
     */
    public function __construct(EarnBalance ...$earnBalances)
    {
        $this->earnBalances = $earnBalances;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->earnBalances);
    }

    public function count(): int
    {
        return count($this->earnBalances);
    }
}
