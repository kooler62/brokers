<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Earn;

class EarnBalance
{
    public function __construct(private readonly string $currency, private readonly float $balance)
    {
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
