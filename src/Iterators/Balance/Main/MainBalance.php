<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Main;

class MainBalance
{
    private float $balance;
    private string $currency;

    public function __construct(string $currency, float $balance)
    {
        $this->balance = $balance;
        $this->currency = $currency;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
