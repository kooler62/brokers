<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Balance\Main;

use ArrayIterator;

class MainBalances implements \IteratorAggregate, \Countable
{
    /**
     * @var MainBalance[]
     */
    private array $mainBalances;

    /**
     * Balances constructor.
     * @param MainBalance ...$mainBalances
     */
    public function __construct(MainBalance ...$mainBalances)
    {
        $this->mainBalances = $mainBalances;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->mainBalances);
    }

    public function count(): int
    {
        return count($this->mainBalances);
    }
}
