<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators;

use Kooler62\Brokers\Enums\MarketType;
use Kooler62\Brokers\Enums\TransferStatus;

class Transfer
{
    private ?int $transferId;
    private MarketType $from;
    private MarketType $to;
    private string $currency;
    private float $amount;
    private TransferStatus $status;

    public function __construct(array $data)
    {
        $this->transferId = isset($data['transfer_id']) ? (int)$data['transfer_id'] : null;
        $this->from = MarketType::fromValue($data['from']);
        $this->to = MarketType::fromValue($data['to']);
        $this->amount = (float)$data['amount'];
        $this->currency = $data['currency'];
        $this->status = TransferStatus::fromValue($data['status']);
    }

    public function getFrom(): MarketType
    {
        return $this->from;
    }

    public function getTo(): MarketType
    {
        return $this->to;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getStatus(): TransferStatus
    {
        return $this->status;
    }

    public function getTransferId(): ?int
    {
        return $this->transferId;
    }
}
