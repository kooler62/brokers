<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\BestBidAsk;

class BestBidAsk
{
    private int $timestamp;
    private string $pair;
    private float $bestBidPrice;
    private float $bestAskPrice;
    private float $bestBidVolume;
    private float $bestAskVolume;
    private float $spread;

    public function __construct(array $data)
    {
        $this->timestamp = (int)$data['timestamp'];
        $this->pair = $data['pair'];
        $this->bestBidPrice = (float)$data['bestBid']['price'];
        $this->bestAskPrice = (float)$data['bestAsk']['price'];
        $this->bestBidVolume = (float)$data['bestBid']['volume'];
        $this->bestAskVolume = (float)$data['bestAsk']['volume'];
        $this->spread = ($this->bestAskPrice - $this->bestBidPrice) / $this->bestAskPrice * 100;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getPair(): string
    {
        return $this->pair;
    }

    public function getBestBidPrice(): float
    {
        return $this->bestBidPrice;
    }

    public function getBestAskPrice(): float
    {
        return $this->bestAskPrice;
    }

    public function getBestBidVolume(): float
    {
        return $this->bestBidVolume;
    }

    public function getBestAskVolume(): float
    {
        return $this->bestAskVolume;
    }

    public function getSpread(): float
    {
        return $this->spread;
    }
}
