<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Trades;

class Trade
{
    private int $orderId;
    private ?string $customOrderId;
    private int $tradeId;
    private ?bool $isMaker;
    private float $price;
    private float $volume;
    private ?float $fee;
    private ?string $feeCurrency;

    public function __construct(array $data)
    {
        $this->tradeId = (int)$data['trade_id'];
        $this->orderId = (int)$data['order_id'];
        $this->customOrderId = $data['custom_order_id'] ?? null;
        $this->isMaker = $data['is_maker'] ?? null;
        $this->price = (float)$data['price'];
        $this->volume = (float)$data['volume'];
        $this->fee = (float)$data['fee'] ?? null;
        $this->feeCurrency = $data['feeCurrency'] ?? null;
    }

    public function getTradeId(): int
    {
        return $this->tradeId;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getCustomOrderId(): ?string
    {
        return $this->customOrderId;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function getIsMaker(): ?bool
    {
        return $this->isMaker;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function getFeeCurrency(): ?string
    {
        return $this->feeCurrency;
    }
}
