<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Trades;

use ArrayIterator;

class Trades implements \IteratorAggregate, \Countable
{
    private int $count;
    /** @var Trade[] */
    private array $trades;

    public function __construct(int $count, array $trades)
    {
        $this->count = count($trades);

        foreach ($trades as $trade) {
            $this->trades[] = new Trade($trade);
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Trade[]
     */
    public function getTrades(): array
    {
        return $this->trades;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->trades);
    }

    public function count(): int
    {
        return count($this->trades);
    }
}
