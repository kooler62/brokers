<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Markets;

use ArrayIterator;

class Markets implements \IteratorAggregate, \Countable
{
    /**
     * @var Market[]
     */
    private array $markets;

    /**
     * Balances constructor.
     * @param Market ...$markets
     */
    public function __construct(Market ...$markets)
    {
        $this->markets = $markets;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->markets);
    }

    public function count(): int
    {
        return count($this->markets);
    }
}
