<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Markets;

class Market
{
    public function __construct(
        private readonly string $name, //pretty name
        private readonly string $pair, //key to use in api
        private readonly string $baseAsset,
        private readonly string $quoteAsset,
        private readonly int $basePrecision = 0,
        private readonly int $quotePrecision = 0,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPair(): string
    {
        return $this->pair;
    }

    public function getBaseAsset(): string
    {
        return $this->baseAsset;
    }

    public function getQuoteAsset(): string
    {
        return $this->quoteAsset;
    }

    public function getBasePrecision(): int
    {
        return $this->basePrecision;
    }

    public function getQuotePrecision(): int
    {
        return $this->quotePrecision;
    }
}
