<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Brokers;

class Brokers implements \IteratorAggregate, \Countable
{
    /**
     * @var Broker[]
     */
    private array $brokers;

    public function __construct(array $brokers)
    {
        foreach ($brokers as $broker) {
            $this->brokers[] = new Broker($broker['name'], $broker['separator']);
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->brokers);
    }

    public function count(): int
    {
        return count($this->brokers);
    }
}
