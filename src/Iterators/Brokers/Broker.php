<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Iterators\Brokers;

class Broker
{
    private string $name;
    private string $separator;

    public function __construct(string $name, string $separator)
    {
        $this->name = $name;
        $this->separator = $separator;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }
}
