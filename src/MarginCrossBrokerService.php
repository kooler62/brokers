<?php

declare(strict_types=1);

namespace Kooler62\Brokers;

use Kooler62\Brokers\Enums\Platform;
use Kooler62\Brokers\Handlers\Binance\Margin\Cross\BinanceCross;
use Kooler62\Brokers\Handlers\None\Margin\Cross\NoneCross;
use Kooler62\Brokers\Handlers\WhiteBit\Margin\Cross\WhiteBitCross;
use Kooler62\Brokers\Interfaces\CrossBrokerInterface;
use Kooler62\Brokers\Traits\GetBrokersTrait;

class MarginCrossBrokerService
{
    use GetBrokersTrait;

    /**
     * @var CrossBrokerInterface[]
     */
    private static array $instance = [];

    /**
     * @param string $broker
     * @return CrossBrokerInterface
     */
    public static function getInstance(string $broker = Platform::BINANCE): CrossBrokerInterface
    {
        if (!isset(self::$instance[$broker])) {
            self::$instance[$broker] = match ($broker) {
                Platform::BINANCE => new BinanceCross(),
                Platform::WHITEBIT => new WhiteBitCross(),
                default => new NoneCross(),
            };
        }

        return self::$instance[$broker];
    }
}
