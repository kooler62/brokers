<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Traits;

use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Kooler62\Brokers\Handlers\Binance\Binance;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

trait BinanceRequestWrapperTrait
{
    public function getSig(array $params): string
    {
        $query = http_build_query($params);

        return hash_hmac('sha256', $query, $this->privateKey);
    }

    /**
     * @throws GuzzleException
     * @throws InvalidMarketException
     */
    public function requestWrapper($method, $uri, $params = [])
    {
        return $this->privateGuzzleWrapper($method, $uri, $params);
    }

    /**
     * @throws GuzzleException
     * @throws InvalidMarketException
     */
    private function privateGuzzleWrapper($method, $uri, $params = [])
    {
        $params['timestamp'] = Binance::getTimestamp();
        $params['signature'] = $this->getSig($params);
        $response = $this->client->request(
            $method,
            $uri . '?' . http_build_query($params),
            [
                'timeout' => 10,
                'headers' => [
                    'User-Agent' => 'Mozilla/4.0 (compatible; PHP Binance API)',
                    'X-MBX-APIKEY' => $this->publicKey,
                ],
            ]
        );

        $json = json_decode((string)$response->getBody(), true);

        $this->handlerErrors($json, $uri, $params);

        return $json;
    }

    /**
     * @throws InvalidMarketException
     * @throws NotEnoughBalanceException
     * @throws LessMinOrderException
     * @throws Exception
     */
    private function handlerErrors(array $response, string $uri, array $params = [])
    {
        if (isset($response['code'])) {
            $error = $response['code'];

            if ($error === -1100) {
                throw new InvalidMarketException($params['symbol']);
            }

            if ($error === -2010) {
                throw new NotEnoughBalanceException($params['symbol']);
            }

            if ($error === -1013) {
                throw new LessMinOrderException($params['symbol']);
            }

            throw new Exception(json_encode($response));
        }
    }
}
