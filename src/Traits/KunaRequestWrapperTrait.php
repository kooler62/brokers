<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Traits;

use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

trait KunaRequestWrapperTrait
{
    /**
     * @throws GuzzleException
     * @throws InvalidMarketException
     */
    public function requestWrapper($uri, $params = [], $method = 'POST')
    {
        return $this->privateGuzzleWrapper($uri, $params, $method);
    }

    /**
     * @throws GuzzleException
     * @throws InvalidMarketException
     */
    private function privateGuzzleWrapper(string $uri = '/v3/auth/me', $params = [], $method = 'POST')
    {
        $nonce = time() * 1000;
        $bodyToSignature = json_encode($params);
        $signature = hash_hmac(
            'sha384',
            utf8_encode("{$uri}{$nonce}{$bodyToSignature}"),
            utf8_encode($this->privateKey)
        );

        $response = $this->client->request($method, $uri, [
            'headers' => [
                'content-type' => 'application/json',
                'kun-apikey' => $this->publicKey,
                'kun-signature' => $signature,
                'kun-nonce' => $nonce,
            ],
            'json' => $params,
        ]);

        if ($response->getStatusCode() === 403) {
            throw new Exception("Maybe your Ip address not in white list, create new api key or change your Ip address");
        }

        $json = json_decode((string)$response->getBody(), true);

        $this->handlerErrors($json, $uri, $params);

        return $json;
    }

    /**
     * @throws Exception
     */
    private function handlerErrors(array $response, string $uri, array $params = []): void
    {
        if (isset($response['messages'])) {
            $error = $response['messages'][0];

            if ($error === 'not_found' && $uri == '/v3/auth/kuna_codes/redeem') {
                return ;
            }

            if ($error === 'market_does_not_have_a_valid_value') {
                throw new InvalidMarketException("in uri $uri");
            }

            if ($error === 'insufficient_funds') {
                throw new NotEnoughBalanceException("in " . $params['symbol']);
            }

            if ($error === 'price_must_be_greater_than_0') {
                throw new LessMinOrderException("in " . $params['symbol']);
            }

            if ($error === 'signature_is_incorrect') {
                throw new LessMinOrderException("in " . $params['symbol']);
            }

            if ($error === 'the_tonce_has_already_been_used_by_access_key') {
                throw new Exception("tonce is the same in this requests");
            }

            if ($error === 'for_pro_members_only') {
                throw new Exception('you tariff is not pro');
            }

            throw new Exception(json_encode($response));
        }
    }
}
