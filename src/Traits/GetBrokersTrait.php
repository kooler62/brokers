<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Traits;

use Kooler62\Brokers\Enums\Platform;
use Kooler62\Brokers\Iterators\Brokers\Brokers;

trait GetBrokersTrait
{
    public static function getBrokers(): Brokers
    {
        return new Brokers([
            [
                'name' => Platform::BINANCE,
                'separator' => '',
            ],
            [
                'name' => Platform::KUNA,
                'separator' => '',
            ],
            [
                'name' => Platform::WHITEBIT,
                'separator' => '',
            ],
        ]);
    }
}
