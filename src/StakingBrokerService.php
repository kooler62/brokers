<?php

declare(strict_types=1);

namespace Kooler62\Brokers;

use Kooler62\Brokers\Enums\Platform;
use Kooler62\Brokers\Handlers\Binance\Staking\BinanceStaking;
use Kooler62\Brokers\Handlers\None\Staking\NoneStaking;
use Kooler62\Brokers\Interfaces\StakeInterface;
use Kooler62\Brokers\Traits\GetBrokersTrait;

class StakingBrokerService
{
    use GetBrokersTrait;

    /**
     * @var StakeInterface[]
     */
    private static array $instance = [];

    /**
     * @param string $broker
     * @return StakeInterface
     */
    public static function getInstance(string $broker = Platform::BINANCE): StakeInterface
    {
        if (!isset(self::$instance[$broker])) {
            self::$instance[$broker] = match ($broker) {
                Platform::BINANCE => new BinanceStaking(),
                default => new NoneStaking(),
            };
        }

        return self::$instance[$broker];
    }
}
