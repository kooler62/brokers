<?php

declare(strict_types=1);

namespace Kooler62\Brokers;

use Kooler62\Brokers\Enums\Platform;
use Kooler62\Brokers\Handlers\Kuna\Code\KunaCode;
use Kooler62\Brokers\Handlers\WhiteBit\Code\WhiteBitCode;
use Kooler62\Brokers\Interfaces\CodeInterface;
use Kooler62\Brokers\Traits\GetBrokersTrait;

class CodeBrokerService
{
    use GetBrokersTrait;

    /**
     * @var CodeInterface[]
     */
    private static array $instance = [];

    /**
     * @param string $broker
     * @return CodeInterface
     */
    public static function getInstance(string $broker = Platform::WHITEBIT): CodeInterface
    {
        if (!isset(self::$instance[$broker])) {
            self::$instance[$broker] = match ($broker) {
                Platform::WHITEBIT, => new WhiteBitCode(),
                Platform::KUNA => new KunaCode(),
            };
        }

        return self::$instance[$broker];
    }
}
