<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Interfaces;

use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalances;

interface CrossBrokerInterface
{
    public function auth(string $publicKey, string $privateKey): self;

    public function getBalances(): CrossBalances;

    //
    //public function transfer(TransferBalanceDTO $transferBalanceDTO): string;
    //
    //public function getMaxBorrow(string $tokenName): float;
    //
    //public function getInterestRate(string $tokenName): TokenCrossInterest;
    //
    //public function getOrderDetails(string $orderId, string $pairName);
    //
    //public function newOrder(NewOrderDTO $dto): string;
    //
    //public function cancel(string $orderId, string $pairName): self;
}
