<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Interfaces;

use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\Ticker\Tickers;

interface UserRestApiInterface
{
    public function auth(string $publicKey, string $privateKey): self;

    public function accountInfo(): AccountInfo;

    public function getSpotBalance(): SpotBalances;

    public function cancelOrder(string $market, string $orderId);

    public function orderBook(string $market);

    public function allDayStatistic(array $markets = []): Tickers;
}
