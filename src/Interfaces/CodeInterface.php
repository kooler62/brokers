<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Interfaces;

use Kooler62\Brokers\DTO\NewCodeDTO;
use Kooler62\Brokers\Iterators\Codes\Code;
use Kooler62\Brokers\Iterators\Codes\Codes;

interface CodeInterface
{
    public function auth(string $publicKey, string $privateKey): self;

    public function createCode(NewCodeDTO $codeDTO): Code;

    public function applyCode(string $code, ?string $passphrase = null): Code;

    public function getMyCodes(): Codes;

    public function getActivatedByMeCodes(): Codes;
}
