<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Interfaces;

use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalances;

interface StakeInterface
{
    public function auth(string $publicKey, string $privateKey): self;

    public function getEarnBalance(?string $currency = null): EarnBalances;
}
