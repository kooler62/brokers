<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Interfaces;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\DTO\TransferDTO;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalances;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Kooler62\Brokers\Iterators\Transfer;

interface BrokerInterface
{
    public function auth(string $publicKey, string $privateKey): BrokerInterface;

    public function accountInfo();

    public function getOrderBook(?string $market = null): array;

    public function getMainBalance(?string $currency = null): MainBalances;

    public function getSpotBalance(?string $currency = null): SpotBalances;

    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO;

    public function cancelOrder(string $market, string $orderId);

    public function getActiveOrders(string $market, ?string $orderId = null, string $customOrderId = null): Orders;

    public function getCompletedOrders(string $market, ?string $orderId = null, string $customOrderId = null): Orders;

    public function getOrdersHistory(string $market, int $limit = null);

    public function getOrderTrades(string $orderId, string $market): Trades;

    public function getPairs(): MarketPairs;

    public function getBidAsk(string $market): BestBidAsk;

    public function getBidAskPrices(): array;

    public function allDayStatistic(array $markets = []): Tickers;

    public function transfer(TransferDTO $dto): Transfer;
}
