<?php

declare(strict_types=1);

namespace Kooler62\Brokers;

use Illuminate\Support\ServiceProvider;

class BrokersProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
