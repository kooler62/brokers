<?php

declare(strict_types=1);

namespace Kooler62\Brokers\DTO;

use Kooler62\Brokers\Enums\OrderSide;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;

class OrderDTO
{
    private string $orderId;
    private ?string $customOrderId;

    private float $price;
    private float $volume;
    private string $market;
    private OrderSide $side;
    private OrderType $type;
    private OrderStatus $status;

    public function __construct(array $data)
    {
        $this->orderId = $data['order_id'];
        $this->customOrderId = $data['custom_order_id'] ?? null;
        $this->price = (float)$data['price'];
        $this->volume = (float)$data['volume'];
        $this->side = OrderSide::fromValue($data['side']);
        $this->status = OrderStatus::fromValue($data['status']);
        $this->market = $data['market'];
        $this->type = OrderType::fromValue($data['type']);
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getCustomOrderId(): ?string
    {
        return $this->customOrderId;
    }

    public function getSide(): OrderSide
    {
        return $this->side;
    }

    public function setSide($side): static
    {
        $this->side = $side;

        return $this;
    }

    public function getType(): OrderType
    {
        return $this->type;
    }

    public function setType($type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getStatus(): OrderStatus
    {
        return $this->status;
    }

    public function setStatus($status): static
    {
        $this->status = $status;

        return $this;
    }

    public function setPrice($price): static
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function setVolume($volume): static
    {
        $this->volume = $volume;

        return $this;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function setMarket($market): static
    {
        $this->market = $market;
        return $this;
    }
}
