<?php

declare(strict_types=1);

namespace Kooler62\Brokers\DTO;

class NewCodeDTO
{
    private ?string $publicComment;
    private ?string $privateComment;
    private ?string $passphrase;
    private ?string $recipient;

    public function __construct(private float $amount, private string $currency)
    {
        $this->publicComment = null;
        $this->privateComment = null;
        $this->passphrase = null;
        $this->recipient = null;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getPublicComment(): ?string
    {
        return $this->publicComment;
    }

    public function getPrivateComment(): ?string
    {
        return $this->privateComment;
    }

    public function setPublicComment(?string $publicComment): static
    {
        $this->publicComment = $publicComment;

        return $this;
    }

    public function setPrivateComment(?string $privateComment): static
    {
        $this->privateComment = $privateComment;

        return $this;
    }

    public function getPassphrase(): ?string
    {
        return $this->passphrase;
    }

    public function setPassphrase(?string $passphrase): static
    {
        $this->passphrase = $passphrase;

        return $this;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setRecipient(?string $recipient): static
    {
        $this->recipient = $recipient;

        return $this;
    }
}
