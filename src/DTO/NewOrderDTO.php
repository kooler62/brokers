<?php

declare(strict_types=1);

namespace Kooler62\Brokers\DTO;

use Kooler62\Brokers\Enums\OrderSide;
use Kooler62\Brokers\Enums\OrderType;

class NewOrderDTO
{
    private float $price;
    private float $volume;
    private string $market;
    private OrderSide $side;
    private OrderType $type;

    public function __construct()
    {
    }

    public function getType(): OrderType
    {
        return $this->type;
    }

    public function setType(OrderType $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getSide(): OrderSide
    {
        return $this->side;
    }

    public function setSide(OrderSide $side): static
    {
        $this->side = $side;
        return $this;
    }

    public function setPrice($price): static
    {
        $this->price = $price;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function setVolume($volume): static
    {
        $this->volume = $volume;

        return $this;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function setMarket($market): static
    {
        $this->market = $market;

        return $this;
    }
}
