<?php

declare(strict_types=1);

namespace Kooler62\Brokers\DTO;

use Kooler62\Brokers\Enums\MarketType;

class TransferDTO
{
    private MarketType $from;
    private MarketType $to;
    private string $currency;
    private float $amount;

    public function __construct(array $data)
    {
        $this->from = MarketType::fromValue($data['from']);
        $this->to = MarketType::fromValue($data['to']);
        $this->currency = $data['currency'];
        $this->amount = (float)$data['amount'];
    }

    public function getFrom(): MarketType
    {
        return $this->from;
    }

    public function getTo(): MarketType
    {
        return $this->to;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}
