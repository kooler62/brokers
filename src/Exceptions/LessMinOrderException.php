<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Exceptions;

use Exception;
use Throwable;

class LessMinOrderException extends Exception
{
    public function __construct(string $message, $code = 0, Throwable $previous = null)
    {
        $title = "Order have small amount. ";

        parent::__construct("$title $message", $code, $previous);
    }
}
