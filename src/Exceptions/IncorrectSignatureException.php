<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Exceptions;

use Exception;
use Throwable;

class IncorrectSignatureException extends Exception
{
    public function __construct(string $message, $code = 0, Throwable $previous = null)
    {
        $title = "signature is incorrect. ";

        parent::__construct("$title $message", $code, $previous);
    }
}
