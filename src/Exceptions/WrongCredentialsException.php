<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Exceptions;

use Exception;
use Throwable;

class WrongCredentialsException extends Exception
{
    public function __construct(string $message, $code = 0, Throwable $previous = null)
    {
        $title = "WrongCredentialsException. ";

        parent::__construct("$title $message", $code, $previous);
    }
}
