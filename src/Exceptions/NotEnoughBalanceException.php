<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Exceptions;

use Exception;
use Throwable;

class NotEnoughBalanceException extends Exception
{
    public function __construct(string $message, $code = 0, Throwable $previous = null)
    {
        $title = "Not enough balance. ";

        parent::__construct("$title $message", $code, $previous);
    }
}
