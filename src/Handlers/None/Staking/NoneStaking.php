<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\None\Staking;

use Kooler62\Brokers\Interfaces\StakeInterface;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalances;
use Kooler62\Brokers\Traits\BinanceRequestWrapperTrait;
use GuzzleHttp\Client;

class NoneStaking implements StakeInterface
{
    use BinanceRequestWrapperTrait;

    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function __construct()
    {
        $this->client = new Client([]);
    }

    public function auth(string $publicKey, string $privateKey): StakeInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function getEarnBalance(?string $currency = null): EarnBalances
    {
        return new EarnBalances(...[]);
    }
}
