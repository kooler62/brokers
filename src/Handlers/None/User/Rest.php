<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\None\User;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\DTO\TransferDTO;
use Kooler62\Brokers\Enums\MarketType;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;
use Kooler62\Brokers\Enums\TransferStatus;
use Kooler62\Brokers\Interfaces\UserRestApiInterface;
use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalances;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalances;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItems;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Kooler62\Brokers\Iterators\Transfer;
use Kooler62\Brokers\Traits\BinanceRequestWrapperTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Rest implements UserRestApiInterface
{
    use BinanceRequestWrapperTrait;

    //public const ENDPOINT_API = 'https://api.binance.com';
    private string $publicKey;
    private string $privateKey;
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([]);
    }

    public function auth(string $publicKey, string $privateKey): UserRestApiInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function getOrderBook(string $market): array
    {
        return [
            'bids' => new OrderBookItems([]),
            'asks' => new OrderBookItems([]),
        ];
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#account-information-user_data */
    public function accountInfo(): AccountInfo
    {
        $me = $this->requestWrapper('GET', '/api/v3/account', []);

        $data = [
            'maker_fee' => $me['commissionRates']['maker'],
            'taker_fee' => $me['commissionRates']['taker'],
        ];

        return new AccountInfo($data);
    }

    public function getSpotBalance(?string $currency = null): SpotBalances
    {
        return new SpotBalances(...[]);
    }

    public function getEarnBalance(?string $currency = null): EarnBalances
    {
        return new EarnBalances(...[]);
    }

    public function getMainBalance(?string $currency = null): MainBalances
    {
        return new MainBalances(...[]);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#new-order-trade */
    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        $data = [
            'symbol' => $newOrderDTO->getMarket(),
            'type' => $newOrderDTO->getType()->value,
            'quantity' => round($newOrderDTO->getVolume(), 5),
            'side' => $newOrderDTO->getSide()->value,
            'price' => $newOrderDTO->getPrice(),
        ];

        if ($newOrderDTO->getType()->value === OrderType::LIMIT) {
            $data['timeInForce'] = 'GTC';
        }

        $response = $this->requestWrapper('POST', '/api/v3/order', $data);

        return new OrderDTO([
            'order_id' => $response['orderId'],
            'custom_order_id' => $response['clientOrderId'],
            'market' => $response['symbol'],
            'side' => $newOrderDTO->getSide(),
            'price' => $response['price'],
//            'status' => strtolower( $response['status']),
            'status' => OrderStatus::ACTIVE,
            'volume' => $response['origQty'],
            'type' => strtolower($response['type']),
        ]);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#cancel-order-trade */
    public function cancelOrder(string $market, string $orderId)
    {
        return $this->requestWrapper('DELETE', '/api/v3/openOrders', ['symbol' => $market]);
    }

    public function getActiveOrders(string $market): Orders
    {
        return new Orders(0, []);
    }

    public function transfer(TransferDTO $dto): Transfer
    {
        $type = '';
        if ($dto->getFrom()->value === MarketType::SPOT && $dto->getTo()->value === MarketType::MAIN) {
            $type = 'MAIN_FUNDING';
        }

        if ($dto->getFrom()->value === MarketType::MAIN && $dto->getTo()->value === MarketType::SPOT) {
            $type = 'FUNDING_MAIN';
        }

        $amount = number_format($dto->getAmount(), 8);

        $transfer = $this->requestWrapper('POST', '/sapi/v1/asset/transfer', [
            'type' => $type,
            'asset' => $dto->getCurrency(),
            'amount' => $amount,
        ]);

        return new Transfer([
            'transfer_id' => $transfer['tranId'],
            'status' => TransferStatus::SUCCESS,
            'from' => $dto->getFrom(),
            'to' => $dto->getTo(),
            'amount' => $amount,
            'currency' => $dto->getCurrency(),
        ]);
    }

    public function getCompletedOrders(string $market, string $orderId = null): Orders
    {
        return new Orders(0, []);
    }

    public function getOrderTrades(?string $orderId, string $market): Trades
    {
        return new Trades(0, []);
    }

    public function exchangeInfo(array $symbols = [], array $permissions = [])
    {
        $uri = "/api/v3/exchangeInfo";

        $data = [];
        if (count($symbols) > 0) {
            $data['symbols'] = json_encode($symbols);
        }

        if (count($symbols) === 0 && count($permissions) > 0) {
            $data['permissions'] = json_encode($permissions);
        }

        $response = $this->client->get($uri . '?' . http_build_query($data));

        return json_decode((string)$response->getBody(), true);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#order-book
     * @throws GuzzleException
     */
    public function orderBook(string $market, int $limit = 100)
    {
        $uri = "/api/v3/depth";

        $data = [
            'symbol' => $market,
            'limit' => $limit,
        ];

        $response = $this->client->get($uri . '?' . http_build_query($data));

        return json_decode((string)$response->getBody(), true);
    }

    /**
     * @throws GuzzleException
     */
    public function getBidAsk(string $market): BestBidAsk
    {
        $bidAsks = $this->orderBook($market, 1);
        $priceField = '0';
        $volumeField = '1';

        return new BestBidAsk([
            'pair' => $market,
            'timestamp' => $bidAsks['lastUpdateId'],
            'bestBid' => [
                'price' => $bidAsks['bids'][0][$priceField],
                'volume' => $bidAsks['bids'][0][$volumeField],
            ],
            'bestAsk' => [
                'price' => $bidAsks['asks'][0][$priceField],
                'volume' => $bidAsks['asks'][0][$volumeField],
            ],
        ]);
    }

    public function getBidAskPrices(): array
    {
        $data = [];

        $tickers = $this->allDayStatistic()
            ->getTickers();

        foreach ($tickers as $ticker) {
            $askPrice = $ticker->getAskPrice();
            $bidPrice = $ticker->getBidPrice();

            if ($askPrice > 0 || $bidPrice > 0) {
                $data[$ticker->getMarket()] = [
                    'ask' => $askPrice,
                    'bid' => $bidPrice,
                ];
            }
        }

        return $data;
    }

    public function getPairs(): MarketPairs
    {
        return new MarketPairs(0, []);
    }

    public function AllDayStatistic(array $markets = []): Tickers
    {
        return new Tickers(0, []);
    }
}
