<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\None\Margin\Cross;

use Kooler62\Brokers\Interfaces\CrossBrokerInterface;
use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalances;
use GuzzleHttp\Client;

class NoneCross implements CrossBrokerInterface
{
    private string $publicKey;
    private string $privateKey;
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([]);
    }

    public function auth(string $publicKey, string $privateKey): CrossBrokerInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function getBalances(): CrossBalances
    {
        return new CrossBalances(...[]);
    }
}
