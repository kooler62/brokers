<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\Binance\User;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\DTO\TransferDTO;
use Kooler62\Brokers\Enums\MarketType;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;
use Kooler62\Brokers\Enums\TransferStatus;
use Kooler62\Brokers\Interfaces\UserRestApiInterface;
use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalance;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalances;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalance;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalances;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalance;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\Markets\Market;
use Kooler62\Brokers\Iterators\Markets\Markets;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItem;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItems;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Ticker;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Kooler62\Brokers\Iterators\Transfer;
use Kooler62\Brokers\Traits\BinanceRequestWrapperTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Rest implements UserRestApiInterface
{
    use BinanceRequestWrapperTrait;

    public const ENDPOINT_API = 'https://api.binance.com';
    private string $publicKey;
    private string $privateKey;
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'http_errors' => false,
                'timeout' => 30,
            ]
        );
    }

    public function auth(string $publicKey, string $privateKey): UserRestApiInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#exchange-information */
    public function markets(): Markets
    {
        $response = $this->client->get("/api/v3/exchangeInfo");
        $markets = [];

        $result = json_decode((string)$response->getBody(), true);
        foreach ($result['symbols'] as $market) {
            $markets[] = new Market(
                name: $market['baseAsset'] . '_' . $market['quoteAsset'],
                pair: $market['baseAsset'] . $market['quoteAsset'],
                baseAsset: $market['baseAsset'],
                quoteAsset: $market['quoteAsset'],
                basePrecision: (int)$market['baseAssetPrecision'],
                quotePrecision: (int)$market['quotePrecision']
            );
        }

        return new Markets(...$markets);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#account-information-user_data */
    public function accountInfo(): AccountInfo
    {
        $me = $this->requestWrapper('GET', '/api/v3/account', []);

        $data = [
            'maker_fee' => $me['commissionRates']['maker'],
            'taker_fee' => $me['commissionRates']['taker'],
        ];

        return new AccountInfo($data);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#account-information-user_data */
    public function getSpotBalance(?string $currency = null): SpotBalances
    {
        $payload = $this->requestWrapper('GET', '/api/v3/account', []);
        $result = [];
        foreach ($payload['balances'] as $balance) {
            $result[] = new SpotBalance(
                (float)$balance['free'] + (float)$balance['locked'],
                (float)$balance['free'],
                (float)$balance['locked'],
                $balance['asset']
            );
        }

        return new SpotBalances(...$result);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#get-staking-product-position-user_data */
    public function getEarnBalance(?string $currency = null): EarnBalances
    {
        $payload = $this->requestWrapper('GET', '/sapi/v1/staking/position', ['product' => 'STAKING']);
        $result = [];
        foreach ($payload as $balance) {
            $result[] = new EarnBalance(
                $balance['asset'],
                (float)$balance['amount'],
            );
        }

        return new EarnBalances(...$result);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#funding-wallet-user_data */
    public function getMainBalance(?string $currency = null): MainBalances
    {
        $payload = $this->requestWrapper('POST', '/sapi/v1/asset/get-funding-asset', []);

        $result = [];
        foreach ($payload as $balance) {
            $result[] = new MainBalance(
                $balance['asset'],
                (float)$balance['free'],
            );
        }

        return new MainBalances(...$result);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#new-order-trade */
    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        $data = [
            'symbol' => $newOrderDTO->getMarket(),
            'type' => $newOrderDTO->getType()->value,
            'quantity' => round($newOrderDTO->getVolume(), 5),
            'side' => $newOrderDTO->getSide()->value,
            'price' => $newOrderDTO->getPrice(),
        ];

        if ($newOrderDTO->getType()->value === OrderType::LIMIT) {
            $data['timeInForce'] = 'GTC';
        }

        $response = $this->requestWrapper('POST', '/api/v3/order', $data);

        return new OrderDTO([
            'order_id' => $response['orderId'],
            'custom_order_id' => $response['clientOrderId'],
            'market' => $response['symbol'],
            'side' => $newOrderDTO->getSide(),
            'price' => $response['price'],
//            'status' => strtolower( $response['status']),
            'status' => OrderStatus::ACTIVE,
            'volume' => $response['origQty'],
            'type' => strtolower($response['type']),
        ]);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#cancel-order-trade */
    public function cancelOrder(string $market, string $orderId)
    {
        return $this->requestWrapper('DELETE', '/api/v3/openOrders', ['symbol' => $market]);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#current-open-orders-user_data */
    public function getActiveOrders(string $market): Orders
    {
        $params = [];
        if ($market !== '*' || strtoupper($market) !== "ALL") {
            $params = ['symbol' => $market];
        }

        $orders = $this->requestWrapper('GET', '/api/v3/openOrders', $params);
        $data = [];
        foreach ($orders as $order) {
            $status = match (strtolower($order['status'])) {
                'new' => OrderStatus::ACTIVE,
                default => strtolower(strtolower($order['status']))
            };

            $data[] = [
                'order_id' => $order['orderId'],
                'market' => $order['symbol'],
                'custom_order_id' => $order['clientOrderId'],
                'volume' => $order['origQty'],
                'executed_volume' => $order['executedQty'],
                'price' => $order['price'],
                'type' => strtolower($order['type']),
                'status' => $status,
                'side' => strtolower($order['side']),
            ];
        }

        return new Orders(count($data), $data);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#user-universal-transfer-user_data */
    public function transfer(TransferDTO $dto): Transfer
    {
        $type = '';
        if ($dto->getFrom()->value === MarketType::SPOT && $dto->getTo()->value === MarketType::MAIN) {
            $type = 'MAIN_FUNDING';
        }

        if ($dto->getFrom()->value === MarketType::MAIN && $dto->getTo()->value === MarketType::SPOT) {
            $type = 'FUNDING_MAIN';
        }

        $amount = number_format($dto->getAmount(), 8);

        $transfer = $this->requestWrapper('POST', '/sapi/v1/asset/transfer', [
            'type' => $type,
            'asset' => $dto->getCurrency(),
            'amount' => $amount,
        ]);

        return new Transfer([
            'transfer_id' => $transfer['tranId'],
            'status' => TransferStatus::SUCCESS,
            'from' => $dto->getFrom(),
            'to' => $dto->getTo(),
            'amount' => $amount,
            'currency' => $dto->getCurrency(),
        ]);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#account-trade-list-user_data */
    public function getCompletedOrders(string $market, string $orderId = null): Orders
    {
        $orders = $this->requestWrapper('GET', '/api/v3/allOrders', [
            'symbol' => $market,
            'orderId' => $orderId,
            'limit' => 500,
        ]);

        $data = [];
        foreach ($orders as $order) {
            $status = strtolower($order['status']);
            if ($status === 'new') {
                $status = OrderStatus::ACTIVE;
            }

            $newOrderDada = [
                'order_id' => $order['orderId'],
                'market' => $order['symbol'],
                'custom_order_id' => $order['clientOrderId'],
                'volume' => $order['origQty'],
                'executed_volume' => $order['executedQty'],
                'price' => $order['price'],
                'type' => strtolower($order['type']),
                'status' => strtolower($status),
                'side' => strtolower($order['side']),
            ];

            if (is_null($orderId) === true) {
                $data[] = $newOrderDada;
            }

            if (is_null($orderId) === false && $order['orderId'] === $orderId) {
                $data[] = $newOrderDada;
            }
        }

        return new Orders(count($data), $data);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#account-trade-list-user_data */
    public function getOrderTrades(?string $orderId, string $market): Trades
    {
        $params = ['symbol' => $market, 'limit' => 500];

        if (is_null($orderId) === false) {
            $params['orderId'] = $orderId;
        }

        $trades = $this->requestWrapper('GET', '/api/v3/myTrades', $params);

        $data = [];
        foreach ($trades as $trade) {
            $data[] = [
                'trade_id' => $trade['id'],
                'order_id' => $trade['orderId'],
                'volume' => $trade['qty'],
                'price' => $trade['price'],
                'feeCurrency' => $trade['commissionAsset'],
                'fee' => $trade['commission'],
                'is_maker' => $trade['isMaker'],
            ];
        }

        return new Trades(count($data), $data);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#exchange-information */
    public function exchangeInfo(array $symbols = [], array $permissions = [])
    {
        $uri = "/api/v3/exchangeInfo";

        $data = [];
        if (count($symbols) > 0) {
            $data['symbols'] = json_encode($symbols);
        }

        if (count($symbols) === 0 && count($permissions) > 0) {
            $data['permissions'] = json_encode($permissions);
        }

        $response = $this->client->get($uri . '?' . http_build_query($data));

        return json_decode((string)$response->getBody(), true);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#order-book */
    public function getOrderBook(string $market): array
    {
        $response = $this->client->get("/api/v3/depth?symbol=$market");
        $result = json_decode((string)$response->getBody(), true);

        $priceKey = 0;
        $volumeKey = 1;

        $bids = [];
        $asks = [];

        foreach ($result['bids'] as $item) {
            $bids[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }

        foreach ($result['asks'] as $item) {
            $asks[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }

        return [
            'bids' => new OrderBookItems($bids),
            'asks' => new OrderBookItems($asks),
        ];
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#order-book
     * @throws GuzzleException
     */
    public function orderBook(string $market, int $limit = 100)
    {
        $uri = "/api/v3/depth";

        $data = [
            'symbol' => $market,
            'limit' => $limit,
        ];

        $response = $this->client->get($uri . '?' . http_build_query($data));

        return json_decode((string)$response->getBody(), true);
    }

    /**
     * @throws GuzzleException
     */
    public function getBidAsk(string $market): BestBidAsk
    {
        $bidAsks = $this->orderBook($market, 1);
        $priceField = '0';
        $volumeField = '1';

        return new BestBidAsk([
            'pair' => $market,
            'timestamp' => $bidAsks['lastUpdateId'],
            'bestBid' => [
                'price' => $bidAsks['bids'][0][$priceField],
                'volume' => $bidAsks['bids'][0][$volumeField],
            ],
            'bestAsk' => [
                'price' => $bidAsks['asks'][0][$priceField],
                'volume' => $bidAsks['asks'][0][$volumeField],
            ],
        ]);
    }

    public function getBidAskPrices(): array
    {
        $data = [];

        $tickers = $this->allDayStatistic()
            ->getTickers();

        foreach ($tickers as $ticker) {
            $askPrice = $ticker->getAskPrice();
            $bidPrice = $ticker->getBidPrice();

            if ($askPrice > 0 || $bidPrice > 0) {
                $data[$ticker->getMarket()] = [
                    'ask' => $askPrice,
                    'bid' => $bidPrice,
                ];
            }
        }

        return $data;
    }

    public function getPairs(): MarketPairs
    {
        $spot = strtoupper(MarketType::SPOT);
        $tickers = $this->exchangeInfo([], [$spot]);

        $data = [];
        foreach ($tickers['symbols'] as $value) {
            if ($value['status'] === 'TRADING') {
                $data[] = [
                    'symbol' => $value['symbol'],
                    'baseCurrency' => $value['baseAsset'],
                    'quoteCurrency' => $value['quoteAsset'],
                ];
            }
        }

        return new MarketPairs(count($data), $data);
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#24hr-ticker-price-change-statistics */
    public function AllDayStatistic(array $markets = []): Tickers
    {
        $uri = "/api/v3/ticker/24hr";

        $data = [];
        if (count($markets) > 0) {
            $data['symbols'] = json_encode($markets);
        }

        $response = $this->client->get($uri . '?' . http_build_query($data));
        $result = json_decode((string)$response->getBody(), true);

        $data = [];

        foreach ($result as $ticker) {
            $data[] = new Ticker([
                'market' => $ticker['symbol'],
                'bidPrice' => $ticker['bidPrice'],
                'bidVolume' => $ticker['bidQty'],
                'askPrice' => $ticker['askPrice'],
                'askVolume' => $ticker['askQty'],
                'priceChange' => $ticker['priceChange'],
                'priceChangePercent' => $ticker['priceChangePercent'],
                'lastPrice' => $ticker['lastPrice'],
                'highPrice' => $ticker['highPrice'],
                'lowPrice' => $ticker['lowPrice'],
            ]);
        }

        return new Tickers(count($data), $data);
    }
}
