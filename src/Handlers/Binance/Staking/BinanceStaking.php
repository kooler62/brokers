<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\Binance\Staking;

use Kooler62\Brokers\Interfaces\StakeInterface;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalance;
use Kooler62\Brokers\Iterators\Balance\Earn\EarnBalances;
use Kooler62\Brokers\Traits\BinanceRequestWrapperTrait;
use GuzzleHttp\Client;

class BinanceStaking implements StakeInterface
{
    use BinanceRequestWrapperTrait;

    public const ENDPOINT_API = 'https://api.binance.com';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'timeout' => 30,
            ]
        );
    }

    public function auth(string $publicKey, string $privateKey): StakeInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#get-staking-product-position-user_data */
    public function getEarnBalance(?string $currency = null): EarnBalances
    {
        //@todo try to refactor with one query
        $payload1 = $this->requestWrapper('GET', '/sapi/v1/staking/position', ['product' => 'STAKING']);
        $payload2 = $this->requestWrapper('GET', '/sapi/v1/staking/position', ['product' => 'F_DEFI']);

        $result = [];
        foreach ($payload1 as $balance) {
            $result[] = new EarnBalance(
                $balance['asset'],
                (float)$balance['amount'],
            );
        }
        foreach ($payload2 as $balance) {
            $result[] = new EarnBalance(
                $balance['asset'],
                (float)$balance['amount'],
            );
        }

        return new EarnBalances(...$result);
    }
}
