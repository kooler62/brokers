<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\Binance\Margin\Cross;

use Kooler62\Brokers\Handlers\Binance\Binance;
use Kooler62\Brokers\Interfaces\CrossBrokerInterface;
use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalance;
use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalances;
use GuzzleHttp\Client;

class BinanceCross implements CrossBrokerInterface
{
    private const ENDPOINT_API = 'https://api.binance.com';
    private string $publicKey;
    private string $privateKey;
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'timeout' => 30,
            ]
        );
    }

    public function auth(string $publicKey, string $privateKey): CrossBrokerInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    /** @url https://binance-docs.github.io/apidocs/spot/en/#query-cross-margin-account-details-user_data */
    public function getBalances(): CrossBalances
    {
        $params['timestamp'] = Binance::getTimestamp();
        $params['signature'] = $this->getSig($params);
        $response = $this->client->get(
            '/sapi/v1/margin/account' . '?' . http_build_query($params),
            [
                'timeout' => 10,
                'headers' => [
                    'User-Agent' => 'Mozilla/4.0 (compatible; PHP Binance API)',
                    'X-MBX-APIKEY' => $this->publicKey,
                ],
            ]
        );
        $payload = json_decode(
            $response->getBody()
                ->getContents(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $result = [];
        foreach ($payload['userAssets'] as $balance) {
            $result[] = new CrossBalance(
                currency: $balance['asset'],
                balance: (float)$balance['free'] + (float)$balance['locked'],
                available: (float)$balance['free'],
                borrowed: (float)$balance['borrowed'],
                interest: (float)$balance['interest'],
            );
        }

        return new CrossBalances(...$result);
    }

    private function getSig(array $params): string
    {
        $query = http_build_query($params);

        return hash_hmac('sha256', $query, $this->privateKey);
    }
}
