<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\WhiteBit\Margin\Cross;

use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Kooler62\Brokers\Exceptions\WrongCredentialsException;
use Kooler62\Brokers\Interfaces\CrossBrokerInterface;
use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalance;
use Kooler62\Brokers\Iterators\Balance\MarginCross\CrossBalances;
use Exception;
use GuzzleHttp\Client;

class WhiteBitCross implements CrossBrokerInterface
{
    public const ENDPOINT_API = 'https://whitebit.com';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'timeout' => 30,
            ]
        );
        //        $this->setInstruments();
    }

    public function auth(string $publicKey, string $privateKey): CrossBrokerInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#collateral-account-balance */
    public function getBalances(?string $currency = null): CrossBalances
    {
        $params = $currency ? ['ticker' => strtoupper($currency)] : [];
        $result = $this->requestWrapper('/api/v4/collateral-account/balance', $params);
        $balances = [];

        foreach ($result as $token => $balance) {
            $balances[] = new CrossBalance(
                $token,
                (float)$balance,
                (float)$balance,
                0,
                0,
            );
        }

        return new CrossBalances(...$balances);
    }

    public function requestWrapper($uri, $params = [])
    {
        return $this->privateCurlWrapper($uri, $params);
    }

    /**
     * @throws InvalidMarketException
     * @throws \Exception
     */
    private function privateCurlWrapper($uri, $params = []): mixed
    {
        $apiKey = $this->publicKey; //put here your public key
        $apiSecret = $this->privateKey; //put here your secret key
        $baseUrl = self::ENDPOINT_API; //domain without last slash. Do not use https://whitebit.com/
        //If the nonce is similar to or lower than the previous request number, you will receive the 'too many requests' error message
        $nonce = (string)(int)(microtime(
            true
        ) * 1000); //nonce is a number that is always higher than the previous request number
        $nonceWindow = true; //boolean, enable nonce validation in time range of current time +/- 5s, also check if nonce value is unique

        $data = [
            //'ticker' => 'BTC', //for example for obtaining trading balance for BTC currency
            'request' => $uri,
            'nonce' => $nonce,
            'nonceWindow' => $nonceWindow,
        ];

        $data = array_merge($data, $params);

        //preparing request URL
        $completeUrl = $baseUrl . $uri;
        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $apiSecret);

        //preparing headers
        $headers = [
            'Content-type: application/json',
            'X-TXC-APIKEY:' . $apiKey,
            'X-TXC-PAYLOAD:' . $payload,
            'X-TXC-SIGNATURE:' . $signature,
        ];

        $connect = curl_init($completeUrl);
        curl_setopt($connect, CURLOPT_POSTFIELDS, $dataJsonStr);
        curl_setopt($connect, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);

        $apiResponse = curl_exec($connect);
        curl_close($connect);

        //receiving data
        $jsonArrayResponse = json_decode($apiResponse);
        if (isset($jsonArrayResponse->code)) {
            if ($jsonArrayResponse->code === 0) {
                throw new WrongCredentialsException('sdsdsdsds');
            }

            if ($jsonArrayResponse->code === 30) {
                $message = $jsonArrayResponse->message;
                $description = json_encode(data_get($jsonArrayResponse, 'errors'));
                throw new \Exception($message . ' | ' . $description);
            }
        }

        $this->handlerErrors((array)$jsonArrayResponse, $uri, $data);

        return (array)$jsonArrayResponse;
    }

    /**
     * @throws InvalidMarketException
     */
    private function handlerErrors(array $response, string $uri, array $params)
    {
        if (isset($response['code'])) {
            $error = $response['code'];

            if ($error === 31) {
                throw new InvalidMarketException($params['market']);
            }

            if ($error === 10) {
                throw new NotEnoughBalanceException($params['market']);
            }

            if ($error === 32) {
                throw new LessMinOrderException($params['market']);
            }

            throw new Exception(json_encode($response));
        }
    }
}
