<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\WhiteBit;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\DTO\TransferDTO;
use Kooler62\Brokers\Handlers\WhiteBit\User\Rest;
use Kooler62\Brokers\Interfaces\BrokerInterface;
use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalances;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\Markets\Markets;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Kooler62\Brokers\Iterators\Transfer;

class WhiteBit implements BrokerInterface
{
    private Rest $userRest;

    public function __construct()
    {
        $this->userRest = new Rest();
    }

    public function markets(): Markets
    {
        return $this->userRest->markets();
    }

    public function auth(string $publicKey, string $privateKey): BrokerInterface
    {
        $this->userRest->auth($publicKey, $privateKey);

        return $this;
    }

    public function getOrderBook(?string $market = null): array
    {
        return $this->userRest->getOrderBook($market);
    }

    public function accountInfo(): AccountInfo
    {
        return $this->userRest->accountInfo();
    }

    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        return $this->userRest->makeOrder($newOrderDTO);
    }

    public function cancelOrder(string $market, string $orderId): OrderDTO
    {
        return $this->userRest->cancelOrder($market, $orderId);
    }

    public function getActiveOrders(string $market, ?string $orderId = null, string $customOrderId = null): Orders
    {
        return $this->userRest->getActiveOrders($market, $orderId, $customOrderId);
    }

    public function getCompletedOrders(string $market, ?string $orderId = null, string $customOrderId = null): Orders
    {
        return $this->userRest->getCompletedOrders($market, $orderId, $customOrderId);
    }

    public function getOrdersHistory(string $market, $limit = null)
    {
        return $this->userRest->getOrdersHistory($market, $limit);
    }

    public function getOrderTrades(string $orderId, string $market): Trades
    {
        return $this->userRest->getOrderTrades($orderId, $market);
    }

    public function getSpotBalance(?string $currency = null): SpotBalances
    {
        return $this->userRest->getSpotBalance($currency);
    }

    public function getMainBalance(?string $currency = null): MainBalances
    {
        return $this->userRest->getMainBalance($currency);
    }

    public function getPairs(): MarketPairs
    {
        return $this->userRest->getPairs();
    }

    public function getBidAsk(string $market): BestBidAsk
    {
        return $this->userRest->getBidAsk($market);
    }

    public function getBidAskPrices(): array
    {
        return $this->userRest->getBidAskPrices();
    }

    public function allDayStatistic(array $markets = []): Tickers
    {
        return $this->userRest->allDayStatistic($markets);
    }

    public function transfer(TransferDTO $dto): Transfer
    {
        return $this->userRest->transfer($dto);
    }
}
