<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\WhiteBit\User;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\DTO\TransferDTO;
use Kooler62\Brokers\Enums\MarketType;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;
use Kooler62\Brokers\Enums\TransferStatus;
use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Kooler62\Brokers\Interfaces\UserRestApiInterface;
use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalance;
use Kooler62\Brokers\Iterators\Balance\Main\MainBalances;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalance;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\Markets\Market;
use Kooler62\Brokers\Iterators\Markets\Markets;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItem;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItems;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Ticker;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Kooler62\Brokers\Iterators\Transfer;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Rest implements UserRestApiInterface
{
    public const ENDPOINT_API = 'https://whitebit.com';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'timeout' => 30,
            ]
        );
    }

    public function markets(): Markets
    {
        $response = $this->client->get("/api/v4/public/markets");
        $markets = [];

        $result = json_decode((string)$response->getBody(), true);

        foreach ($result as $market) {
            $markets[] = new Market(
                name: $market['stock'] . '_' . $market['money'],
                pair: $market['stock'] . '_' . $market['money'],
                baseAsset: $market['stock'],
                quoteAsset: $market['money'],
                basePrecision: (int)$market['stockPrec'],
                quotePrecision: (int)$market['moneyPrec']
            );
        }

        return new Markets(...$markets);
    }

    public function auth(string $publicKey, string $privateKey): UserRestApiInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function accountInfo(): AccountInfo
    {
        $balances = $this->requestWrapper('/api/v4/main-account/balance', []);
        $wbtHold = (float)data_get($balances, 'WBT-HOLD.main_balance');
        $wbt = (float)data_get($balances, 'WBT.main_balance');

        return new AccountInfo($this->feeGrid($wbt, $wbtHold));
    }

    /** @url https://whitebit-exchange.github.io/api-docs/public/http-v4/#orderbook */
    public function getOrderBook(string $market): array
    {
        $response = $this->client->get("/api/v4/public/orderbook/$market");
        $result = json_decode((string)$response->getBody(), true);

        $priceKey = 0;
        $volumeKey = 1;

        $bids = [];
        $asks = [];

        foreach ($result['bids'] as $item) {
            $bids[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }

        foreach ($result['asks'] as $item) {
            $asks[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }

        return [
            'bids' => new OrderBookItems($bids),
            'asks' => new OrderBookItems($asks),
        ];
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#trading-balance */
    public function getSpotBalance(?string $currency = null): SpotBalances
    {
        $params = $currency ? ['ticker' => strtoupper($currency)] : [];
        $result = $this->requestWrapper('/api/v4/trade-account/balance', $params);
        $balances = [];
        if (is_null($currency) === true) {
            foreach ($result as $token => $balance) {
                $freeze = (float)$balance->freeze;
                $available = (float)$balance->available;
                $total = $freeze + $available;

                $balances[] = new SpotBalance(
                    $total,
                    $available,
                    $freeze,
                    $token
                );
            }
        }

        if (is_null($currency) === false) {
            $freeze = (float)$result['freeze'];
            $available = (float)$result['available'];
            $total = $freeze + $available;

            $balances[] = new SpotBalance(
                $total,
                $available,
                $freeze,
                $currency
            );
        }

        return new SpotBalances(...$balances);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#main-balance */
    public function getMainBalance(?string $currency = null): MainBalances
    {
        $params = $currency ? ['ticker' => strtoupper($currency)] : [];
        $result = $this->requestWrapper('/api/v4/main-account/balance', $params);
        $balances = [];

        if (is_null($currency) === true) {
            foreach ($result as $token => $balance) {
                $balances[] = new MainBalance($token, (float)$balance->main_balance);
            }
        }

        if (is_null($currency) === false) {
            $balances[] = new MainBalance($currency, (float)$result['main_balance']);
        }

        return new MainBalances(...$balances);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#create-limit-order
     * @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#create-market-order
     * @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#create-stock-market-order
     * @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#create-stop-limit-order
     * @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#create-stop-market-order
     */
    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        if ($newOrderDTO->getType()->value === OrderType::LIMIT) {
            return $this->makeLimitOrder($newOrderDTO->setType(OrderType::LIMIT()));
        }

        return $this->makeMarketOrder($newOrderDTO->setType(OrderType::MARKET()));
    }

    private function makeLimitOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        $response = $this->requestWrapper('/api/v4/order/new', [
            'market' => $newOrderDTO->getMarket(),
            'side' => $newOrderDTO->getSide(),
            'price' => $newOrderDTO->getPrice(),
            'amount' => $newOrderDTO->getVolume(),
            'clientOrderId' => self::customOrderIdGenerator($newOrderDTO->getMarket()),

            //Orders are guaranteed to be the maker order when executed. Variables: 'true' / 'false' Example: 'false'.
            'postOnly' => true,
        ]);

        return new OrderDTO([
            'order_id' => (string)$response['orderId'],
            'custom_order_id' => $response['clientOrderId'] === '' ? null : $response['clientOrderId'],
            'market' => $response['market'],
            'side' => $response['side'],
            'price' => $response['price'],
            'volume' => $response['amount'],
            'type' => $response['type'],
            'status' => OrderStatus::ACTIVE,
        ]);
    }

    private function makeMarketOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        $response = $this->requestWrapper('/api/v4/order/market', [
            'market' => $newOrderDTO->getMarket(),
            'side' => $newOrderDTO->getSide(),
            //'price' => $newOrderDTO->getPrice(),
            'amount' => $newOrderDTO->getVolume(),
            'clientOrderId' => self::customOrderIdGenerator($newOrderDTO->getMarket()),
            //Orders are guaranteed to be the maker order when executed. Variables: 'true' / 'false' Example: 'false'.
            //'postOnly' => true,
        ]);

        return new OrderDTO([
            'order_id' => (string)$response['orderId'],
            'custom_order_id' => $response['clientOrderId'] === '' ? null : $response['clientOrderId'],
            'market' => $response['market'],
            'side' => $response['side'],
            'price' => $response['price'],
            'volume' => $response['amount'],
            'type' => $response['type'],
            'status' => OrderStatus::ACTIVE,
        ]);
    }

    public static function customOrderIdGenerator(?string $market): string
    {
        //@todo refactor without carbon
        $dateTime = new DateTime();
        $time = $dateTime->format('YmdHis');
        $microseconds = substr(
            $dateTime->format('u'),
            0,
            3
        );
        $data = [
            $time,
            $microseconds,
            rand(10, 99),
            str($market)->remove('_'),
        ];

        //@todo только буквы и цифры
        return implode('H', array_filter($data));
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#cancel-order */
    public function cancelOrder(string $market, string $orderId): OrderDTO
    {
        $response = $this->requestWrapper('/api/v4/order/cancel', [
            'market' => $market,
            'orderId' => $orderId,
        ]);

        return new OrderDTO([
            'order_id' => $response['orderId'],
            'custom_order_id' => $response['clientOrderId'] === '' ? null : $response['clientOrderId'],
            'market' => $response['market'],
            'side' => $response['side'],
            'type' => $response['type'],
            'volume' => $response['amount'],
            'price' => $response['price'],
            'status' => ($response['amount'] === $response['left']) ? OrderStatus::CANCELED : OrderStatus::PARTIAL,
        ]);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#transfer-between-main-and-trade-balances */
    public function transfer(TransferDTO $dto): Transfer
    {
        $fromValue = MarketType::MAIN;
        $toValue = MarketType::SPOT;

        if ($dto->getFrom()->value === MarketType::SPOT) {
            $fromValue = MarketType::SPOT;
        }

        if ($dto->getFrom()->value === MarketType::MARGIN_CROSS) {
            $fromValue = 'collateral';
        }

        if ($dto->getTo()->value === MarketType::MAIN) {
            $toValue = MarketType::MAIN;
        }

        if ($dto->getTo()->value === MarketType::MARGIN_CROSS) {
            $toValue = 'collateral';
        }

        $amount = number_format($dto->getAmount(), 8);

        $data = [
            'from' => strtolower($fromValue),
            'to' => strtolower($toValue),
            'ticker' => $dto->getCurrency(),
            'amount' => $amount,
        ];

        $this->requestWrapper('/api/v4/main-account/transfer', $data);

        return new Transfer([
            'status' => TransferStatus::SUCCESS,
            'from' => $dto->getFrom()->value,
            'to' => $dto->getTo()->value,
            'currency' => $data['ticker'],
            'amount' => $data['amount'],
        ]);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#query-unexecutedactive-orders */
    public function getActiveOrders(string $market, string $orderId = null, string $customOrderId = null): Orders
    {
        $data = [
            'market' => $market,
            'limit' => 100,
//            'clientOrderId',
//            'orderId'
//            'offset'
        ];

        if (is_null($orderId) === false) {
            $data['orderId'] = $orderId;
        }

        if (is_null($customOrderId) === false && $customOrderId !== '') {
            $data['clientOrderId'] = $customOrderId;
        }

        $orders = $this->requestWrapper('/api/v4/orders', $data);
        $data = [];
        foreach ($orders as $order) {
            $order = (array)$order;
            $data[] = [
                'order_id' => (string)$order['orderId'],
                'market' => $order['market'],
                'custom_order_id' => $order['clientOrderId'],
                'volume' => $order['amount'],
                'executed_volume' => $order['dealStock'],
                'price' => $order['price'],
                'type' => $order['type'],
                'status' => $order['amount'] === $order['dealStock'] ? OrderStatus::FILLED : OrderStatus::PARTIAL,
                'side' => $order['side'],
            ];
        }

        return new Orders(count($data), $data);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#query-executed-orders */
    public function getCompletedOrders(string $market, string $orderId = null, string $customOrderId = null): Orders
    {
        $data = [
            'market' => $market,
            'limit' => 100,
//            'clientOrderId',
//            'orderId'
//            'offset'
        ];

        if (is_null($orderId) === false) {
            $data['orderId'] = $orderId;
        }

        if (is_null($customOrderId) === false && $customOrderId !== '') {
            $data['clientOrderId'] = $customOrderId;
        }

        $rawOrders = $this->requestWrapper('/api/v4/trade-account/order/history', $data);
        $data = [];
        foreach ($rawOrders as $pair => $orders) {
            foreach ($orders as $order) {
                $order = (array)$order;

                $data[] = [
                    'order_id' => (string)$order['id'],
                    'market' => $pair,
                    'custom_order_id' => $order['clientOrderId'],
                    'volume' => $order['amount'],
                    'executed_volume' => $order['dealStock'],
                    'price' => $order['price'],
                    'type' => $order['type'],
                    'status' => $order['amount'] === $order['dealStock'] ? OrderStatus::FILLED : OrderStatus::PARTIAL,
                    'side' => $order['side'],
                ];
            }
        }

        return new Orders(count($data), $data);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#query-executed-order-history
     * @throws Exception
     */
    public function getOrdersHistory(string $market, int $limit = null)
    {
        throw new Exception('not implemented');
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-trade-v4/#query-executed-order-deals */
    public function getOrderTrades(string $orderId, string $market): Trades
    {
        $trades = $this->requestWrapper('/api/v4/trade-account/order', [
            'orderId' => $orderId,
            'limit' => 100,
        ]);

        $whiteBitMakerType = 2;

        $data = [];
        foreach ($trades['records'] as $trade) {
            $trade = (array)$trade;
            $data[] = [
                'trade_id' => $trade['id'],
                'order_id' => $trade['dealOrderId'],
                'custom_order_id' => $trade['clientOrderId'],
                'volume' => $trade['amount'],
                'price' => $trade['price'],
                'fee' => $trade['fee'],
                'is_maker' => $trade['role'] === $whiteBitMakerType,
            ];
        }

        return new Trades(count($data), $data);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/public/http-v4/#market-info
     * @throws GuzzleException
     */
    public function getMarketInfo(): array
    {
        $response = $this->client->get('/api/v4/public/markets');

        return json_decode((string)$response->getBody(), true);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/public/http-v4/#asset-status-list
     * @throws GuzzleException
     */
    public function getTickers(): array
    {
        $response = $this->client->get('/api/v4/public/ticker');

        return json_decode((string)$response->getBody(), true);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/public/http-v4/#orderbook
     * @throws GuzzleException
     */
    public function orderBook(string $market, int $limit = null, int $level = null): array
    {
        $uri = "/api/v4/public/orderbook/$market";
        $data = [];
        if (is_null($limit) === false) {
            $data['limit'] = $limit;
        }

        if (is_null($level) === false) {
            $data['level'] = $level;
        }

        $response = $this->client->get($uri . '?' . http_build_query($data));

        return json_decode((string)$response->getBody(), true);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/public/http-v1/#market-activity
     * @throws GuzzleException
     */
    public function allDayStatistic(array $markets = []): Tickers
    {
        $response = $this->client->get('/api/v1/public/tickers');
        $json = json_decode((string)$response->getBody(), true);

        $data = [];
        foreach ($json['result'] as $key => $item) {
            $ticker = $item['ticker'];
            $data[] = new Ticker([
                'market' => $key,
                'bidPrice' => $ticker['bid'],
                'askPrice' => $ticker['ask'],
                'priceChangePercent' => $ticker['change'],
                'lastPrice' => $ticker['last'],
                'highPrice' => $ticker['high'],
                'lowPrice' => $ticker['low'],
            ]);
        }

        return new Tickers(count($data), $data);
    }

    /**
     * @throws GuzzleException
     */
    public function getBidAsk(string $market): BestBidAsk
    {
        $response = $this->orderBook($market, 1);

        return new BestBidAsk([
            'pair' => $market,
            'timestamp' => $response['timestamp'],
            'bestBid' => [
                'price' => $response['bids'][0][0],
                'volume' => $response['bids'][0][1],
            ],
            'bestAsk' => [
                'price' => $response['asks'][0][0],
                'volume' => $response['asks'][0][1],
            ],
        ]);
    }

    public function getBidAskPrices(): array
    {
        $data = [];

        $tickers = $this->allDayStatistic()
            ->getTickers();

        foreach ($tickers as $ticker) {
            $askPrice = $ticker->getAskPrice();
            $bidPrice = $ticker->getBidPrice();

            if ($askPrice > 0 || $bidPrice > 0) {
                $data[$ticker->getMarket()] = [
                    'ask' => $askPrice,
                    'bid' => $bidPrice,
                ];
            }
        }

        return $data;
    }

    /**
     * @throws GuzzleException
     */
    public function getPairs(): MarketPairs
    {
        $tickers = $this->getMarketInfo();

        $data = [];
        foreach ($tickers as $value) {
            if ($value['type'] === MarketType::SPOT) {
                $data[] = [
                    'symbol' => $value['name'],
                    'baseCurrency' => $value['stock'],
                    'quoteCurrency' => $value['money'],
                ];
            }
        }

        return new MarketPairs(count($data), $data);
    }

    public function requestWrapper($uri, $params = [])
    {
        return $this->privateCurlWrapper($uri, $params);
    }

    private function feeGrid(float $wbt, float $wbtHold): array
    {
        $makerFee = 0.001;
        $takerFee = 0.001;

        switch ($wbt) {
            case ($wbt >= 400):
                $makerFee = 0.0009;
                $takerFee = 0.00095;
                break;
            case ($wbt >= 1_000):
                $makerFee = 0.0008;
                $takerFee = 0.0009;
                break;
            case ($wbt >= 5_000):
                $makerFee = 0.0006;
                $takerFee = 0.00085;
                break;
            case ($wbt >= 10_000):
                $makerFee = 0.0004;
                $takerFee = 0.0008;
                break;
            case ($wbt >= 15_000):
                $makerFee = 0.0002;
                $takerFee = 0.00075;
                break;
            case ($wbt >= 30_000):
                $makerFee = 0.0;
                $takerFee = 0.0007;
                break;
            case ($wbt >= 50_000):
                $makerFee = 0.0;
                $takerFee = 0.00065;
                break;
            case ($wbt >= 100_000):
                $makerFee = 0.0;
                $takerFee = 0.0006;
                break;
            case ($wbt >= 1_000_000):
                $makerFee = 0.0;
                $takerFee = 0.0002;
                break;
            case ($wbt >= 3_000_000):
                $makerFee = 0.0;
                $takerFee = 0.0001;
                break;
        }

        if ($wbtHold >= 200) {
            $makerFee = 0.0;
        }

        return [
            'maker_fee' => $makerFee,
            'taker_fee' => $takerFee,
        ];
    }

    /**
     * @throws InvalidMarketException
     * @throws \Exception
     */
    private function privateCurlWrapper($uri, $params = []): mixed
    {
        //If the nonce is similar to or lower than the previous request number, you will receive the 'too many requests' error message
        $nonce = (string)(int)(microtime(
            true
        ) * 1000); //nonce is a number that is always higher than the previous request number
        $nonceWindow = true; //boolean, enable nonce validation in time range of current time +/- 5s, also check if nonce value is unique

        $data = [
            //'ticker' => 'BTC', //for example for obtaining trading balance for BTC currency
            'request' => $uri,
            'nonce' => $nonce,
            'nonceWindow' => $nonceWindow,
        ];

        $data = array_merge($data, $params);

        //preparing request URL
        $completeUrl = self::ENDPOINT_API . $uri;
        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $this->privateKey);

        //preparing headers
        $headers = [
            'Content-type: application/json',
            'X-TXC-APIKEY:' . $this->publicKey,
            'X-TXC-PAYLOAD:' . $payload,
            'X-TXC-SIGNATURE:' . $signature,
        ];

        $connect = curl_init($completeUrl);
        curl_setopt($connect, CURLOPT_POSTFIELDS, $dataJsonStr);
        curl_setopt($connect, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);

        $apiResponse = curl_exec($connect);
        curl_close($connect);

        //receiving data
        $jsonArrayResponse = json_decode($apiResponse);

        if (isset($jsonArrayResponse->code)) {
            if ($jsonArrayResponse->code === 0) {
                throw new Exception(json_encode($jsonArrayResponse));
            }

            if ($jsonArrayResponse->code === 30) {
                $message = $jsonArrayResponse->message;
                $description = json_encode(data_get($jsonArrayResponse, 'errors'));
                throw new \Exception($message . ' | ' . $description);
            }
        }

        $this->handlerErrors((array)$jsonArrayResponse, $uri, $data);

        return (array)$jsonArrayResponse;
    }

    /**
     * @throws InvalidMarketException
     * @throws NotEnoughBalanceException
     * @throws LessMinOrderException
     * @throws Exception
     */
    private function handlerErrors(array $response, string $uri, array $params)
    {
        if (isset($response['code'])) {
            $error = $response['code'];

            if ($error === 31) {
                throw new InvalidMarketException($params['market']);
            }

            if ($error === 10) {
                throw new NotEnoughBalanceException($params['market']);
            }

            if ($error === 32) {
                throw new LessMinOrderException($params['market']);
            }

            throw new Exception(json_encode($response));
        }
    }
}
