<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\WhiteBit\Code;

use Kooler62\Brokers\DTO\NewCodeDTO;
use Kooler62\Brokers\Enums\CodeStatus;
use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Kooler62\Brokers\Interfaces\CodeInterface;
use Kooler62\Brokers\Iterators\Codes\Code;
use Kooler62\Brokers\Iterators\Codes\Codes;
use Exception;
use GuzzleHttp\Client;

class WhiteBitCode implements CodeInterface
{
    public const ENDPOINT_API = 'https://whitebit.com';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'timeout' => 30,
            ]
        );
    }

    public function auth(string $publicKey, string $privateKey): CodeInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#get-codes-history */
    public function getActivatedByMeCodes(): Codes
    {
        $result = $this->requestWrapper('/api/v4/main-account/codes/history', [
            'limit' => 100,
            'offset' => 0,
        ]);
        $data = [];
        foreach ($result['data'] as $item) {
            $code = (array)$item;
            $data[] = new Code([
                'id' => $code['external_id'],
                'code' => $code['code'],
                'amount' => $code['amount'],
                'currency' => $code['ticker'],
                'status' => $code['status'],
                'code_created_at' => $code['date'],
            ]);
        }

        return new Codes($data);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#get-my-codes */
    public function getMyCodes(): Codes
    {
        $result = $this->requestWrapper('/api/v4/main-account/codes/my', [
            'limit' => 100,
            'offset' => 0,
        ]);
        $data = [];

        foreach ($result['data'] as $item) {
            $code = (array)$item;

            $data[] = new Code([
                'id' => $code['external_id'],
                'code' => $code['code'],
                'amount' => $code['amount'],
                'currency' => $code['ticker'],
                'status' => $code['status'],
                'code_created_at' => $code['date'],
            ]);
        }

        return new Codes($data);
    }

    /** @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#create-code */
    public function createCode(NewCodeDTO $codeDTO): Code
    {
        $amount = $codeDTO->getAmount();

        $data = [
            'ticker' => $codeDTO->getCurrency(),
            'amount' => "$amount",
        ];

        if (is_null($codeDTO->getPrivateComment()) === false) {
            $data['description'] = $codeDTO->getPrivateComment();
        }

        if (is_null($codeDTO->getPassphrase()) === false) {
            $data['passphrase'] = $codeDTO->getPassphrase();
        }

        $result = (array)$this->requestWrapper('/api/v4/main-account/codes', $data);
        $externalId = $result['external_id'];
        $code = $result['code'];

        $myCodes = $this->getMyCodes();

        /** @var Code $myCode */
        foreach ($myCodes as $myCode) {
            if ($externalId === $myCode->getId() && $code === $myCode->getCode()) {
                return $myCode;
            }
        }
    }

    /**  @url https://whitebit-exchange.github.io/api-docs/private/http-main-v4/#apply-code */
    public function applyCode(string $code, ?string $passphrase = null): Code
    {
        $params = ['code' => $code];
        if (is_null($passphrase) === false) {
            $params['passphrase'] = $passphrase;
        }

        $result = (array)$this->requestWrapper('/api/v4/main-account/codes/apply', $params);

        $status = ($result['message'] === 'Code was successfully applied') ? CodeStatus::ACTIVATED : CodeStatus::FAILED;

        return new Code([
            'code' => $code,
            'amount' => $result['amount'] ?? 0,
            'status' => $status,
            'currency' => $result['ticker'] ?? '---',
            'id' => $result['external_id'] ?? null,
        ]);
    }

    public function requestWrapper($uri, $params = [])
    {
        return $this->privateCurlWrapper($uri, $params);
    }

    /**
     * @throws InvalidMarketException
     * @throws Exception
     */
    private function privateCurlWrapper($uri, $params = []): mixed
    {
        $apiKey = $this->publicKey; //put here your public key
        $apiSecret = $this->privateKey; //put here your secret key
        $baseUrl = self::ENDPOINT_API; //domain without last slash. Do not use https://whitebit.com/
        //If the nonce is similar to or lower than the previous request number, you will receive the 'too many requests' error message
        $nonce = (string)(int)(microtime(
            true
        ) * 1000); //nonce is a number that is always higher than the previous request number
        $nonceWindow = true; //boolean, enable nonce validation in time range of current time +/- 5s, also check if nonce value is unique

        $data = [
            //'ticker' => 'BTC', //for example for obtaining trading balance for BTC currency
            'request' => $uri,
            'nonce' => $nonce,
            'nonceWindow' => $nonceWindow,
        ];

        $data = array_merge($data, $params);

        //preparing request URL
        $completeUrl = $baseUrl . $uri;
        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $apiSecret);

        //preparing headers
        $headers = [
            'Content-type: application/json',
            'X-TXC-APIKEY:' . $apiKey,
            'X-TXC-PAYLOAD:' . $payload,
            'X-TXC-SIGNATURE:' . $signature,
        ];

        $connect = curl_init($completeUrl);
        curl_setopt($connect, CURLOPT_POSTFIELDS, $dataJsonStr);
        curl_setopt($connect, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);

        $apiResponse = curl_exec($connect);
        curl_close($connect);

        //receiving data
        $jsonArrayResponse = (array)json_decode($apiResponse);

        if (
            isset($jsonArrayResponse['message']) &&
            $jsonArrayResponse['message'] === 'Code was successfully created'
        ) {
            return $jsonArrayResponse;
        }

        if (isset($jsonArrayResponse['code'])) {
            if (isset($jsonArrayResponse['message']) && data_get(
                $jsonArrayResponse,
                'errors.code.0'
            ) === 'Code is activated, or you passed incorrect data') {
                return $jsonArrayResponse;
            }

            if ($jsonArrayResponse['code'] === 0) {
                throw new Exception("WrongCredentialsException");
            }

            if ($jsonArrayResponse['code'] === 30) {
                $message = $jsonArrayResponse['message'];
                $description = json_encode(data_get($jsonArrayResponse, 'errors'));
                throw new Exception($message . ' | ' . $description);
            }
        }

        $this->handlerErrors((array)$jsonArrayResponse, $uri, $data);

        return (array)$jsonArrayResponse;
    }

    /**
     * @throws InvalidMarketException
     */
    private function handlerErrors(array $response, string $uri, array $params)
    {
        if (isset($response['code'])) {
            $error = $response['code'];

            if ($error === 31) {
                throw new InvalidMarketException($params['market']);
            }

            if ($error === 10) {
                throw new NotEnoughBalanceException($params['market']);
            }

            if ($error === 32) {
                throw new LessMinOrderException($params['market']);
            }

            throw new Exception(json_encode($response));
        }
    }
}
