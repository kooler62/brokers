<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\Kuna\User;

use Kooler62\Brokers\DTO\NewOrderDTO;
use Kooler62\Brokers\DTO\OrderDTO;
use Kooler62\Brokers\Enums\OrderSide;
use Kooler62\Brokers\Enums\OrderStatus;
use Kooler62\Brokers\Enums\OrderType;
use Kooler62\Brokers\Exceptions\InvalidMarketException;
use Kooler62\Brokers\Exceptions\LessMinOrderException;
use Kooler62\Brokers\Exceptions\NotEnoughBalanceException;
use Kooler62\Brokers\Interfaces\UserRestApiInterface;
use Kooler62\Brokers\Iterators\AccountInfo;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalance;
use Kooler62\Brokers\Iterators\Balance\Spot\SpotBalances;
use Kooler62\Brokers\Iterators\BestBidAsk\BestBidAsk;
use Kooler62\Brokers\Iterators\MarketPairs\MarketPairs;
use Kooler62\Brokers\Iterators\Markets\Market;
use Kooler62\Brokers\Iterators\Markets\Markets;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItem;
use Kooler62\Brokers\Iterators\OrderBook\OrderBookItems;
use Kooler62\Brokers\Iterators\Orders\Orders;
use Kooler62\Brokers\Iterators\Ticker\Ticker;
use Kooler62\Brokers\Iterators\Ticker\Tickers;
use Kooler62\Brokers\Iterators\Trades\Trades;
use Exception;
use GuzzleHttp\Client;

class Rest implements UserRestApiInterface
{
    public const ENDPOINT_API = 'https://api.kuna.io';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function auth(string $publicKey, string $privateKey): UserRestApiInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'http_errors' => false,
                'connect_timeout' => 1.9,
            ]
        );
        // $this->setInstruments();
    }

    /** @url https://docs.kuna.io/reference/marketpubliccontroller_getallmarkets */
    public function markets(): Markets
    {
        $response = $this->client->get("v4/markets/public/getAll");
        $markets = [];

        $result = json_decode((string)$response->getBody(), true);

        foreach ($result['data'] as $market) {
            $base = $market['baseAsset']['code'];
            $quote = $market['quoteAsset']['code'];

            $markets[] = new Market(
                name: "{$base}_{$quote}",
                pair: "{$base}_{$quote}",
                baseAsset: $base,
                quoteAsset: $quote,
                basePrecision: (int)$market['baseAsset']['precision'],
                quotePrecision: (int)$market['quoteAsset']['precision']
            );
        }

        return new Markets(...$markets);
    }

    /** @url https://docs.kuna.io/docs/get-public-orders-book */
    public function getOrderBook(string $market): array
    {
        $response1 = $this->client->get("/v4/order/public/book/$market");
        $result = json_decode((string)$response1->getBody(), true);
        $bids = [];
        $asks = [];

        $priceKey = 0;
        $volumeKey = 1;

        foreach ($result['data']['bids'] as $item) {
            $bids[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }
        foreach ($result['data']['asks'] as $item) {
            $asks[] = new OrderBookItem((float)$item[$priceKey], (float)$item[$volumeKey]);
        }

        return [
            'bids' => new OrderBookItems($bids),
            'asks' => new OrderBookItems($asks),
        ];
    }

    /** @url https://docs.kuna.io/docs/get-client-info */
    public function me(): array
    {
        $result = $this->requestWrapperV4('/v4/private/me', 'GET', []);

        return $result['data'];
    }

    public function accountInfo(): AccountInfo
    {
        $makerFee = 0.0025;
        $takerFee = 0.0025;

        $data = [
            'maker_fee' => $makerFee,
            'taker_fee' => $takerFee,
        ];

        return new AccountInfo($data);
    }

    /** @url https://docs.kuna.io/docs/get-client-balance */
    public function getSpotBalance(): SpotBalances
    {
        $balances = [];
        $result = $this->requestWrapperV4('/v4/private/getBalance', 'GET', []);

        foreach ($result['data'] as $tokenBalance) {
            $balances[] = new SpotBalance(
                balance: (float)$tokenBalance['balance'] + (float)$tokenBalance['lockBalance'],
                available: (float)$tokenBalance['balance'],
                order: (float)$tokenBalance['lockBalance'],
                currency: $tokenBalance['currency']
            );
        }

        return new SpotBalances(...$balances);
    }

    /** @url https://docs.kuna.io/docs/get-active-client-orders-private */
    public function getActiveOrders(?string $market = null): Orders
    {
        if (is_null($market) === true || 'all' === strtolower($market)) {
            $orders = $this->requestWrapperV4("/v4/order/private/active", 'GET', []);
        } else {
            $orders = $this->requestWrapperV4("/v4/order/private/active?pairs=$market", 'GET', []);
        }

        $data = [];
        foreach ($orders['data'] as $order) {
            //Canceled, Closed, Expired, Open, Pending, Rejected, WaitStop
            $status = match (strtolower($order['status'])) {
                'open' => OrderStatus::ACTIVE,
                'closed' => OrderStatus::CANCELED,
                'executed' => OrderStatus::FILLED,
                default => strtolower($order['status'])
            };

            $data[] = [
                'order_id' => $order['id'],
                'market' => $order['pair'],
                'volume' => (float)$order['quantity'],
                'executed_volume' => (float)$order['executedQuantity'],
                'price' => $order['price'],
                'type' => strtolower($order['type']),
                'status' => $status,
                'side' => ($order['side'] === 'Bid') ? OrderSide::BUY : OrderSide::SELL,
            ];
        }

        return new Orders(count($data), $data);
    }

    /** @url https://docs.kuna.io/docs/get-private-orders-history */
    public function getCompletedOrders(?string $market = null): Orders
    {
        $url = "/v4/order/private/history?pairs=$market";

        if (is_null($market) === true || 'all' === strtolower($market)) {
            $url = "/v4/order/private/history";
        }

        $orders = $this->requestWrapperV4($url, 'GET', ['limit' => 100]);
        $data = [];
        foreach ($orders['data'] as $order) {
            //Canceled, Closed, Expired, Open, Pending, Rejected, WaitStop
            $status = match (strtolower($order['status'])) {
                'open' => OrderStatus::ACTIVE,
                'closed' => OrderStatus::CANCELED,
                'executed' => OrderStatus::FILLED,
                default => strtolower($order['status'])
            };

            $data[] = [
                'order_id' => $order['id'],
                'market' => $order['pair'],
                'volume' => (float)$order['quantity'],
                'executed_volume' => (float)$order['executedQuantity'],
                'price' => $order['price'],
                'type' => strtolower($order['type']),
                'status' => $status,
                'side' => ($order['side'] === 'Bid') ? OrderSide::BUY : OrderSide::SELL,
            ];
        }

        return new Orders(count($orders), $data);
    }

    /** @url https://docs.kuna.io/docs/create-a-new-order-private */
    public function makeOrder(NewOrderDTO $newOrderDTO): OrderDTO
    {
        $data = [
            'pair' => $newOrderDTO->getMarket(),
            'quantity' => (string)$newOrderDTO->getVolume(),
            'orderSide' => $newOrderDTO->getSide()->value === OrderSide::BUY ? 'Bid' : 'Ask',
        ];

        if ($newOrderDTO->getType()->value === OrderType::LIMIT) {
            $data['type'] = 'Limit';
        }
        if ($newOrderDTO->getType()->value === OrderType::MARKET) {
            $data['type'] = 'Market';
        }

        if ($newOrderDTO->getType()->value === OrderType::LIMIT) {
            $data['price'] = (string)$newOrderDTO->getPrice();
        }

        $response = $this->requestWrapperV4("/v4/order/private/create", 'POST', $data);
        $result = $response['data'];

        $status = strtolower($result['status']);

        if ($status === 'pending' || $status === 'open') {
            $status = OrderStatus::ACTIVE;
        }

        return new OrderDTO([
            'order_id' => $result['id'],
            'market' => $result['pair'],
            'side' => $newOrderDTO->getSide(),
            'price' => $result['price'],
            'status' => $status,
            'volume' => $result['quantity'],
            'type' => strtolower($result['type']),
        ]);
    }

    /** @url https://docs.kuna.io/docs/cancel-order-by-id */
    public function cancelOrder(string $market, string $orderId)
    {
        return $this->requestWrapperV4('/v4/order/private/cancel', 'POST', ['orderId' => $orderId]);
    }

    /** @url https://docs.kuna.io/docs/get-trades-by-order-id */
    public function getOrderTrades(string $orderId, string $market = null, $limit = null): Trades
    {
        $trades = $this->requestWrapperV4("/v4/order/private/{$orderId}/trades", 'GET', []);

        $data = [];

        foreach ($trades['data'] as $trade) {
            $data[] = [
                'trade_id' => $trade['id'],
                'order_id' => $trade['orderId'],
                'volume' => (float)$trade['quantity'],
                'price' => (float)$trade['price'],
                'is_maker' => $trade['isTaker'] !== true,
                'fee' => (float)$trade['fee'],
                'feeCurrency' => $trade['feeCurrency'],
            ];
        }

        return new Trades(count($data), $data);
    }

    /** @url https://docs.kuna.io/docs/get-market-info-by-tickers */
    public function getTickers(array $markets): array
    {
        $marketsString = implode(',', $markets);

        $response = $this->client->get("/v4/markets/public/tickers?pairs={$marketsString}");
        $result = json_decode((string)$response->getBody(), true);

        return $result['data'];
    }

    /** @url https://docs.kuna.io/docs/get-public-orders-book */
    public function orderBook(string $market): array
    {
        $response = $this->client->get("/v4/order/public/book/$market");
        $result = json_decode((string)$response->getBody(), true);

        return $result['data'];
    }

    /** @url https://docs.kuna.io/docs/get-time-on-the-server */
    public function serverTime(): array
    {
        $response = $this->client->get("/v4/public/timestamp");

        $json = json_decode((string)$response->getBody(), true);

        return $json['data'];
    }

    /** @url https://docs.kuna.io/docs/get-market-info-by-tickers */
    public function allDayStatistic(array $markets = []): Tickers
    {
        if (count($markets) === 0) {
            foreach ($this->markets() as $market) {
                $markets[] = $market->getname();
            }
        }

        $symbols = implode(',', $markets);

        $response = $this->client->get("/v4/markets/public/tickers?pairs=$symbols");
        $result = json_decode((string)$response->getBody(), true);
        $data = [];

        foreach ($result['data'] as $ticker) {
            $data[] = new Ticker([
                'market' => $ticker['pair'],
                'bidPrice' => $ticker['bestBidPrice'],
//                'bidVolume' => $ticker[$bidVolumeKey],
                'askPrice' => $ticker['bestAskPrice'],
//                'askVolume' => $ticker[$askVolumeKey],
                'priceChange' => $ticker['priceChange'],
                'priceChangePercent' => $ticker['percentagePriceChange'],
                'lastPrice' => $ticker['price'],
                'highPrice' => $ticker['high'],
                'lowPrice' => $ticker['low'],
            ]);
        }

        return new Tickers(count($data), $data);
    }

    public function getBidAsk(string $market): BestBidAsk
    {
        $bidAsks = $this->orderBook($market);
        $bids = $bidAsks['bids'];
        $asks = $bidAsks['asks'];

        $timestamp = $this->serverTime();
        $priceField = '0';
        $volumeField = '1';

        return new BestBidAsk([
            'pair' => $market,
            'timestamp' => $timestamp['timestamp'],
            'bestBid' => [
                'price' => $bids[$priceField],
                'volume' => $bids[$volumeField],
            ],
            'bestAsk' => [
                'price' => $asks[$priceField],
                'volume' => $asks[$volumeField],
            ],
        ]);
    }

    public function getBidAskPrices(): array
    {
        $data = [];

        $tickers = $this->allDayStatistic()
            ->getTickers();

        foreach ($tickers as $ticker) {
            $askPrice = $ticker->getAskPrice();
            $bidPrice = $ticker->getBidPrice();

            if ($askPrice > 0 || $bidPrice > 0) {
                $data[$ticker->getMarket()] = [
                    'ask' => $askPrice,
                    'bid' => $bidPrice,
                ];
            }
        }

        return $data;
    }

    public function getPairs(): MarketPairs
    {
        $pairs = ['BTC_UAH'];

        $tickers = $this->getTickers();
        $data = [];

        foreach ($tickers as $value) {
            $data[] = $this->parsePair($value[0]);
        }

        return new MarketPairs($data);
    }

    private function parsePair(string $pair): array
    {
        $quotes = ['uah', 'usdt', 'usdt', 'usdc'];

        $quoteCurrency = $baseCurrency = '';

        foreach ($quotes as $quote) {
            if (str_ends_with($pair, $quote)) {
                $quoteCurrency = $quote;
            }
        }

        $baseCurrency = str_replace($quoteCurrency, '', $pair);

        return [
            'symbol' => $pair,
            'baseCurrency' => $baseCurrency,
            'quoteCurrency' => $quoteCurrency,
        ];
    }

    public function requestWrapperV4($uri, $method, $params = [])
    {
        return $this->privateGuzzleWrapperV4($uri, $method, $params);
    }

    private function privateGuzzleWrapperV4(string $uri = '/v4/auth/me', string $method, $params = [])
    {
        $nonce = time() * 1000;
        $bodyToSignature = json_encode($params);
        $signature = hash_hmac(
            'sha384',
            utf8_encode("{$uri}{$nonce}{$bodyToSignature}"),
            utf8_encode($this->privateKey)
        );

        $response = $this->client->request($method, $uri, [
            'headers' => [
                'content-type' => 'application/json',
                'public-key' => $this->publicKey,
                'signature' => $signature,
                'nonce' => $nonce,
            ],
            'json' => $params,
        ]);

        $json = json_decode((string)$response->getBody(), true);

        $this->handlerErrors($json, $uri, $params);

        return $json;
    }

    /**
     * @throws Exception
     */
    private function handlerErrors(array $response, string $uri, array $params = [])
    {
        if (isset($response['messages'])) {
            $error = $response['messages'][0];

            if ($error === 'market_does_not_have_a_valid_value') {
                throw new InvalidMarketException("in uri $uri");
            }

            if ($error === 'insufficient_funds') {
                throw new NotEnoughBalanceException("in " . $params['symbol']);
            }

            if ($error === 'price_must_be_greater_than_0') {
                throw new LessMinOrderException("in " . $params['symbol']);
            }

            if ($error === 'signature_is_incorrect') {
                throw new LessMinOrderException("in " . $params['symbol']);
            }

            if ($error === 'the_tonce_has_already_been_used_by_access_key') {
                throw new Exception("tonce is the same in this requests");
            }

            if ($error === 'for_pro_members_only') {
                throw new Exception('you tariff is not pro');
            }

            throw new Exception(json_encode($response));
        }
    }
}
