<?php

declare(strict_types=1);

namespace Kooler62\Brokers\Handlers\Kuna\Code;

use Kooler62\Brokers\Enums\CodeStatus;
use Kooler62\Brokers\Interfaces\CodeInterface;
use Kooler62\Brokers\Iterators\Codes\Code;
use Kooler62\Brokers\Iterators\Codes\Codes;
use Kooler62\Brokers\Traits\KunaRequestWrapperTrait;
use GuzzleHttp\Client;
use Kooler62\Brokers\DTO\NewCodeDTO;

class KunaCode implements CodeInterface
{
    use KunaRequestWrapperTrait;

    public const ENDPOINT_API = 'https://api.kuna.io';
    private Client $client;
    private string $publicKey;
    private string $privateKey;

    public function auth(string $publicKey, string $privateKey): CodeInterface
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;

        return $this;
    }

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::ENDPOINT_API,
                'http_errors' => false,
                'timeout' => 30,
            ]
        );
    }

    /** @url https://docs.kuna.io/docs/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C-kuna-code */
    public function createCode(NewCodeDTO $codeDTO): Code
    {
        $data = [
            'amount' => $codeDTO->getAmount(),
            'currency' => $codeDTO->getCurrency(),
        ];

        if (is_null($codeDTO->getPrivateComment()) === false) {
            $data['private_comment'] = $codeDTO->getPrivateComment();
        }
        if (is_null($codeDTO->getPublicComment()) === false) {
            $data['comment'] = $codeDTO->getPublicComment();
        }
        if (is_null($codeDTO->getRecipient()) === false) {
            $data['recipient'] = $codeDTO->getRecipient();
        }

        $code = $this->requestWrapper('/v3/auth/kuna_codes', $data);

        return new Code([
            'id' => $code['id'],
            'sn' => $code['sn'],
            'code' => $code['code'],
            'recipient' => $code['recipient'],
            'amount' => $code['amount'],
            'currency' => $code['currency'],
            'status' => $code['status'],
            'redeemed_at' => $code['redeemed_at'],
            'non_refundable_before' => $code['non_refundable_before'],
            'comment' => $code['comment'],
            'private_comment' => $code['private_comment'],
            'code_created_at' => $code['created_at'],
        ]);
    }

    /** @url https://docs.kuna.io/docs/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B2%D1%8B%D0%BF%D1%83%D1%89%D0%B5%D0%BD%D0%BD%D1%8B%D1%85-%D0%BA%D0%BE%D0%B4%D0%BE%D0%B2 */
    public function getMyCodes(): Codes
    {
        $data = [
            'page' => 1,
            'per_page' => 1,

            //Сортировать по created_at, redeemed_at, amount (по умолчанию created_at)
            'order_by' => 1,

            //Порядок сортировки asc или desc (по умолчанию desc)
            'order_dir' => 1,
            //string[] Фильтровать по статусам created, processing, active, redeeming, redeemed, onhold, canceled
            'status' => 1,

        ];

        $request = $this->requestWrapper('/v3/auth/kuna_codes/issued-by-me', []);

        $data = [];
        foreach ($request['items'] as $code) {
            $data[] = new Code([
                'id' => $code['id'],
                'sn' => $code['sn'],
                'code' => $code['code'],
                'recipient' => $code['recipient'],
                'amount' => $code['amount'],
                'currency' => $code['currency'],
                'status' => $code['status'],
                'redeemed_at' => $code['redeemed_at'],
                'non_refundable_before' => $code['non_refundable_before'],
                'comment' => $code['comment'],
                'private_comment' => $code['private_comment'],
                'code_created_at' => $code['created_at'],
            ]);
        }

        return new Codes($data);
    }

    /** @url https://docs.kuna.io/docs/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B2%D1%8B%D0%BF%D1%83%D1%89%D0%B5%D0%BD%D0%BD%D1%8B%D1%85-%D0%BA%D0%BE%D0%B4%D0%BE%D0%B2 */
    public function getActivatedByMeCodes(): Codes
    {
        $data = [
            'page' => 1,
            'per_page' => 1,

            //Сортировать по created_at, redeemed_at, amount (по умолчанию created_at)
            'order_by' => 1,

            //Порядок сортировки asc или desc (по умолчанию desc)
            'order_dir' => 1,
            //string[] Фильтровать по статусам created, processing, active, redeeming, redeemed, onhold, canceled
            'status' => 1,

        ];

        $request = $this->requestWrapper('/v3/auth/kuna_codes/redeemed-by-me', []);

        $data = [];
        foreach ($request['items'] as $code) {
            $data[] = new Code([
                'id' => $code['id'],
                'sn' => $code['sn'],
                'code' => $code['code'],
                'recipient' => $code['recipient'],
                'amount' => $code['amount'],
                'currency' => $code['currency'],
                'status' => $code['status'],
                'redeemed_at' => $code['redeemed_at'],
                'non_refundable_before' => $code['non_refundable_before'],
                'comment' => $code['comment'],
                'private_comment' => $code['private_comment'],
                'code_created_at' => $code['created_at'],
            ]);
        }

        return new Codes($data);
    }

    /** @url https://docs.kuna.io/docs/%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C-kuna-code */
    public function applyCode(string $code, ?string $passphrase = null): Code
    {
        $response = $this->requestWrapper('/v3/auth/kuna_codes/redeem', ['code' => $code], 'PUT');
        $status = CodeStatus::ERROR;

        if (data_get($response, 'messages.0' === 'not_found')) {
            $status = CodeStatus::NOT_FOUND;
        }

        if ($response['status'] === 'redeeming') {
            $status = CodeStatus::ACTIVATED;
        }

        return new Code([
            'code' => $code,
            'amount' => $response['amount'] ?? 0,
            'status' => $status,
            'currency' => $response['currency'] ?? '---',
            'id' => $response['id'] ?? null,
            'comment' => $response['comment'] ?? null,
        ]);
    }
}
