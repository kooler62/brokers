<?php

declare(strict_types=1);

namespace Kooler62\Brokers;

use Kooler62\Brokers\Enums\Platform;
use Kooler62\Brokers\Handlers\Binance\Binance;
use Kooler62\Brokers\Handlers\Kuna\Kuna;
use Kooler62\Brokers\Handlers\WhiteBit\WhiteBit;
use Kooler62\Brokers\Interfaces\BrokerInterface;
use Kooler62\Brokers\Traits\GetBrokersTrait;

class SpotBrokerService
{
    use GetBrokersTrait;

    /**
     * @var BrokerInterface[]
     */
    private static array $instance = [];

    /**
     * @param string $broker
     * @return BrokerInterface
     */
    public static function getInstance(string $broker = Platform::BINANCE): BrokerInterface
    {
        if (!isset(self::$instance[$broker])) {
            self::$instance[$broker] = match ($broker) {
                Platform::BINANCE => new Binance(),
                Platform::WHITEBIT, => new WhiteBit(),
                Platform::KUNA => new Kuna(),
            };
        }

        return self::$instance[$broker];
    }
}
